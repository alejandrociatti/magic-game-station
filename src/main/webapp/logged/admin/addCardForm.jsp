<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 4/5/13
  Time: 10:19 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/MGS/resources/stylesheet.css" type="text/css" charset="utf-8" />
<html>
<head>
    <title>Card Management!</title>
</head>
<body>
<form action="/MGS/CardManager" method="post">
    <label>Name:</label> <input type="text" name="name"><br>
    <label>CMC:</label> <input type="text" name="CMC"><br>
    <label>Type:</label> <input type="text" name="type"><br>
    <label>Rules Text:</label> <input type="text" name="rules"><br>
    <label>Flavor Text:</label> <input type="text" name="text"><br>
    <label>Power:</label> <input type="text" name="power"><br>
    <label>Toughness:</label> <input type="text" name="toughness"><br>
    <label>Set/Edition:</label> <input type="text" name="set"><br>
    <label>Rarity:</label> <input type="text" name="rarity"><br>
    <input type="submit" name="action" value="ADD_CARD" class="commonButton">
</form>

<form action="/MGS/CardManager" method="post">
    <input type="submit" name="action" value="LIST_CARDS" class="commonButton">
</form>

<form action="/MGS/CardManager">
    <input type="submit" name="action" value="MODIFY_CARDS" class="commonButton">
</form>

<form action="/MGS/CardManager" method="post">
    <input type="submit" name="action" value="DELETE_CARD" class="commonButton">
</form>






</body>
</html>