<%@ page import="edu.austral.magic.components.Rarity" %>
<%@ page import="edu.austral.magic.components.CardColor" %>
<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 5/8/13
  Time: 8:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/MGS/resources/stylesheet.css" type="text/css" charset="utf-8" />
<html>
<head>
    <title></title>
</head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="/MGS/resources/scripts/resizeIFrame.js"></script>
<script src="/MGS/logged/admin/modifyCardDialog.js"></script>
<script type="text/javascript">
    document.domain = 'localhost'
</script>
<body>
<div class="modifyContainer">
    <div class="modifySearch">
        <iframe class="leftyFrame" id="iFrame" onload="resizeIframe(this)" src="/MGS/logged/search/finder.jsp" seamless>
            <label>"Software shit"-Ivan Vanko (Iron Man 2) </label>
        </iframe>
    </div>

    <div class="wrapper">
        <div class="shadow" id="editionBox">
            <form id="hiddenForm" action="/MGS/CardManager" method="post">
                <label>Name:</label><input type="text" name="name" id="cardName"><br>
                <label>CMC:</label><input type="text" name="CMC" id="cardCMC"><br>
                <label>Type:</label><select name="type">
                    <option value=""></option>
                    <option value="Artifact">Artifact</option>
                    <option value="Basic">Basic</option>
                    <option value="Creature">Creature</option>
                    <option value="Enchantment">Enchantment</option>
                    <option value="Instant">Instant</option>
                    <option value="Land">Land</option>
                    <option value="Legendary">Legendary</option>
                    <option value="Planeswalker">Planeswalker</option>
                    <option value="Snow">Snow</option>
                    <option value="Sorcery">Sorcery</option>
                    <option value="Tribal">Tribal</option>
                </select><br>
                <label>Subtype:</label><input type="text" name="subtype" id="cardSubtype"><br>
                <label>Rules:</label><input type="text" name="rules" id="cardRules"><br>
                <label>Power:</label><input type="text" name="power" id="cardPower"><br>
                <label>Toughness:</label><input type="text" name="toughness" id="cardToughness"><br>
                <label>Set:</label><input type="text" name="set" id="cardSet"><br>
                <label>Rarity:</label><select name="rarity">
                    <option value=""></option>
                    <%
                        Rarity[] rarities= Rarity.values();
                        for (Rarity rarity : rarities) {
                            out.println("<option value=\""+rarity.name()+"\">"+rarity.name()+"</option>");
                        }
                    %>
                </select><br>
                <label>Color:</label><select name="color">
                    <option value=""></option>
                    <%
                        CardColor[] colors = CardColor.values();
                        for (CardColor color : colors) {
                            out.println("<option value=\""+color.name()+"\">"+color.name()+"</option>");
                        }
                    %>
                </select><br>
                <input type="hidden" name="id" id="cardId" class="hiddenInput">
                <input type="submit" name="action" value="MODIFY_CARD" class="commonButton">
            </form>
        </div>
    </div>
</div>
</body>
</html>