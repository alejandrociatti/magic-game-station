/**
 * Created with IntelliJ IDEA.
 * User: Alejandro
 * Date: 13/05/13
 * Time: 05:00
 */

$(document).ready(function(){
    $("#iFrame").load(function(){
        $("#iFrame").contents().find(".cardMini").click(function(){
            var action = "CARD_GET";
            var id;
            id = $(this).data('id');
            $.ajax({
                url: "/MGS/CardManager",
                type: "GET",
                data: "id="+id+"&action="+action,
                success: function(response){
                    $("#cardName").attr("value",response.name);
                    $("#cardCMC").attr("value",response.CMC);
                    $("#cardSet").attr("value",response.magicSet);
                    $("#cardPower").attr("value",response.power);
                    $("#cardToughness").attr("value",response.toughness);
                    $("#cardRules").attr("value",response.rules);
                    $("#cardSubtype").attr("value",response.subType);
                    $("#cardId").attr("value", response.id);

                    $("#hiddenForm").css("display","block");

                }
            })
        });
    });
});
