<%@ page import="edu.austral.magic.components.Card" %>
<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 4/5/13
  Time: 11:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/MGS/resources/stylesheet.css" type="text/css" charset="utf-8" />
<html>

    <head>
    <title>Is this your Card?</title>
    </head>
        <%
            final Card card = (Card) request.getAttribute("card");
        %>
    <body>
        <div class="imgBox">
            <p class="cardName"><%= card.getName()%>     <%= card.getCMC()%></p>
            <p class="cardType"><%= card.getType()%></p>
            <p class="cardRules"><%= card.getRules()%></p>
            <p class="cardText"><%= card.getText()%></p>
            <p class="cardPT"><%= card.getPower()%>/<%= card.getToughness()%></p>
        </div>
    </body>
</html>