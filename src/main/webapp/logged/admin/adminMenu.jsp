<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Magic Game Station</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/mc/crafty.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/css/smoothness/jquery-ui-1.10.3.custom.min.css">
    <style>
        #stroked{text-decoration: line-through}
    </style>
    <link rel="canonical" href=""><!-- http://goo.gl/wKFDI -->
    <link rel=icon href="${pageContext.request.contextPath}/resources/mc/favicon.ico">

    <script src="${pageContext.request.contextPath}/resources/mc/js/modernizr.min.js"></script>
</head>

<body>
<!--[if IE]><div class="ie"><![endif]-->
<div id="wrapper">
    <header class="clearfix container">
        <hgroup>
            <h1><a href="index.jsp" id="big-header">Magic Game Station</a></h1>
            <h2>Let's play some Magic!</h2>
        </hgroup>
        <nav role="navigation">
            <ul>
                <li><a href="${pageContext.request.contextPath}/logged/deckedit/deckList.jsp">Decks</a></li>
                <li><a href="#">Find a Match</a></li>
                <li><a href="#">Quick Join!</a></li>
                <li><a href="${pageContext.request.contextPath}/logged/search/finder.jsp">Search Cards</a></li>
                <li><a href="#" id='log-out-button'>Log Out</a></li>
                <li><a id="admin-button" href="${pageContext.request.contextPath}/logged/admin/adminMenu.jsp">Admin Menu</a></li>
            </ul>
        </nav>
    </header>
    <div class="container">
        <aside role="complimentary" id="complimentary">
            <div class="widget" id="search">
                <div class="widget-content">
                    <%--TODO make this form where it belongs (Post to the servlet)--%>
                    <form class="clearfix">
                        <input type="search" placeholder="_">
                        <input type="hidden" name="action" value="CARD_SEARCH_QUICK">
                        <input class="searchbtn" type="submit" value="Search">
                    </form>
                </div><!-- .widget-content -->
            </div><!-- .widget -->


        </aside><!-- complimentary -->
        <div id="content-before"><!-- Should be empty --></div>
        <div role="main" id="main">
            <article>
                <a href="${pageContext.request.contextPath}/logged/admin/addCardForm.jsp" class='button' id="stroked">Add a Card</a><br>
                <a href="#" class='button'>Load a Set</a><br>
                <a href="${pageContext.request.contextPath}/logged/admin/modifyCards.jsp" class="button">Modify Cards</a>
            </article>

        </div><!-- main -->
        <div id="content-after"><!-- Should be empty --></div>
    </div><!-- .container -->
    <footer>
        <p>This template was created by <a href="http://d3x.co" target="_blank">D3X Solutions</a> and is licensed under <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU GPL 2</a>.</p>
    </footer>
</div><!-- #wrapper -->

<!--[if IE]></div><![endif]-->
<!--Place your JavaScript down here-->
<script src="${pageContext.request.contextPath}/resources/mc/js/fontloader.js"></script><!-- Load custom online fonts http://goo.gl/Deqf0 -->
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/logOutScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/adminMenuUI.js"></script>

</body>
</html>


