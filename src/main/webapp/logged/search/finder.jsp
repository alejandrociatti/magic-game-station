<%@ page import="edu.austral.magic.dao.CardDAO" %>
<%@ page import="edu.austral.magic.hibernate.HibernateUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 4/16/13
  Time: 4:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/stylesheet.css" type="text/css"/>
<html>
<head>
    <title></title>
</head>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/finderScripts.js"></script>
<script>
        document.domain = 'localhost';
</script>
<%
    request.setAttribute("sets", CardDAO.getCardDAO().getSets(HibernateUtil.getGuestSession()));
%>
<body style="background-color: #ececec">
<div class="shadow" id="finder">
    <div class="searchFieldFixed">
        <form class="searchForm" id="search">
            <input id="searchForm" type="text" placeholder="Search Terms..." name="search" style="width:330px"><br>
            <label><b>Search By:</b></label>
            <input type="radio" name="byWhat" value="name" checked>Name
            <input type="radio" name="byWhat" value="type">Types
            <input type="radio" name="byWhat" value="rules">Text
            <div class="searchOptions">
                <label><b>Filter Card Set: </b></label>
                <select name="set">
                    <option value=""></option>
                    <%--@elvariable id="sets" type="java.util.List<java.lang.String>"--%>
                    <c:if test="${sets != null}">
                        <c:forEach items="${sets}" var="set">
                            <option value='${set}'>${set}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
            <div class="searchOptions">
                <label><b>Filter Card Type: </b></label>
                <select name="type">
                    <option value=""></option>
                    <option value="Artifact">Artifact</option>
                    <option value="Basic">Basic</option>
                    <option value="Creature">Creature</option>
                    <option value="Enchantment">Enchantment</option>
                    <option value="Instant">Instant</option>
                    <option value="Land">Land</option>
                    <option value="Legendary">Legendary</option>
                    <option value="Planeswalker">Planeswalker</option>
                    <option value="Snow">Snow</option>
                    <option value="Sorcery">Sorcery</option>
                    <option value="Tribal">Tribal</option>
                </select>
            </div>
            <div>
                <label><b>Filter Card Color: </b></label>
                <input type="checkbox" name="whiteMana">
                <img class="whiteMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                </input>
                <input type="checkbox" name="blueMana">
                <img class="blueMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                </input>
                <input type="checkbox" name="blackMana">
                <img class="blackMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                </input>
                <input type="checkbox" name="redMana">
                <img class="redMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                </input>
                <input type="checkbox" name="greenMana">
                <img class="greenMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                </input>
            </div>
            <div class="searchCheckbox">
                <input type="checkbox" name="exact">Match Colors <b>Exactly</b></input><br>
                <input type="checkbox" name="exclude"><b>Exclude</b> unselected Colors</input>
            </div>
            <input type="hidden" name="action" value="CARD_SEARCH">
    </form>
        <div class="submitButton">
            <button id='tellParent' class="hidden">Better tell my folks!</button>
            <button id="submit">Search</button>
        </div>
        <!--TODO replace this (use script instead)-->
        <div id="results">
        <%--@elvariable id="cards" type="java.util.List<edu.austral.magic.components.Card>"--%>
            <c:if test="${cards != null}">
                <c:forEach items="${cards}" var="card">
                    <div class='cardMini' data-id='${card.id}' data-type='${card.type}'
                            data-name='${card.name}'>
                        ${card.name}
                        <div class='cardDisplay'>
                            <img src="${pageContext.request.contextPath}/resources/${card.magicSet}/${card.name}.full.jpg" class="cardImage">
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>