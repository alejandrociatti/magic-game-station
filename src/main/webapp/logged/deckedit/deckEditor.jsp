<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 5/10/13
  Time: 5:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/stylesheet.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/css/smoothness/jquery-ui-1.10.3.custom.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/css/smoothness/jquery-ui-other.css" type="text/css">

<html>
<head>
    <title></title>
</head>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/json2.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/resizeIFrame.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/logged/deckedit/deckEdit.js"></script>
<script>
    document.domain = 'localhost';
</script>
<body>
<div class="modifyContainer">
    <div class="modifySearch">
        <iframe class="leftyFrame" id="iFrame" src="/MGS/logged/search/finder.jsp" seamless>
            <label>"Software shit"-Ivan Vanko (Iron Man 2) </label>
        </iframe>
    </div>

    <div class="wrapper">
        <div class="shadow" id="editionBox" style="overflow: hidden">
            <div class="editionMenu">
                <button id="add">
                    <img class="add-icon" src="${pageContext.request.contextPath}/resources/img_trans.gif">
                </button><br>
                <button id="add-sideboard">
                    <img class="add-icon-blue" src="${pageContext.request.contextPath}/resources/img_trans.gif">
                </button><br>
            </div>

            <div class="deckList">
                <dl id="land">
                    <dt class="title"><label>Lands</label></dt>
                </dl>
                <dl id="creature">
                    <dt class="title"><label>Creatures</label></dt>
                </dl>
                <dl id="spell">
                    <dt class="title"><label>Spells</label></dt>
                </dl>
                <dl id="sideboard">
                    <dt class="title"><label>Sideboard</label></dt>
                </dl>
            </div>
            <form method="POST" action="${pageContext.request.contextPath}/deckManager" id="deckForm">
                <input type="hidden" id="deckInfo" name="deck">
                <input type="hidden" id='deckName' name='deckName'>
                <input type="hidden" name="action" value="DECK_CREATE">
            </form>
            <button id="submit-deck">Save Deck</button>
        </div>
    </div>
</div>

<div id="dialog-form" title="Set Deck Name">
    <p class="validateTips">All form fields are required.</p>
    <form>
        <fieldset>
            <label for="name">Deck Name</label>
            <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
        </fieldset>
    </form>
</div>


</body>
</html>