/**
 * User: alejandro
 * Date: 5/14/13
 * Time: 11:48 PM
 */

$(document).ready(function(){
    var selectedCardId;
    var selectedCardType;
    var selectedCardName;
    var deckJSON = {
        "deck": [],
        "sideboard": []
    };
    var deckIndex = [];
    var sideboardIndex = [];
    var tips = $('.validateTips');
    var deckName = $('#name');

    //Function used to update messages in submission dialog
    function updateTips(t) {
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
    }

    //Checks if o.val().length is within min & max range
    function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " +
                min + " and " + max + "." );
            return false;
        } else {
            return true;
        }
    }

    //Checks if o.val() complies with the regular expression given
    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass( "ui-state-error" );
            updateTips( n );
            return false;
        } else {
            return true;
        }
    }

    //Sets everything for the submission dialog
    $('#dialog-form').dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "Save Deck": function(){
                var isValid = true;
                deckName.removeClass('ui-state-error');
                isValid = isValid && checkLength(deckName, "deck name", 3, 35);
                isValid = isValid && checkRegexp(deckName,/^[a-zA-Z_0-9 ]+$/i,"deck name may consist of a-z, 0-9 or spaces");
                if(isValid){
                    $('#deckName').val(deckName.val());
                    $(this).dialog('close');
                    //Submit the form containing the deck & its name
                    $('#deckForm').submit();
                }
            },
            Cancel: function(){
                $(this).dialog('close');
            }
        },
        close: function(){
            deckName.val('').removeClass('ui-state-error');
        }
    });

    //When button is clicked, this adds deck to form and displays dialog
    $('#submit-deck').button().click(function(){
        $('#deckInfo').val(JSON.stringify(deckJSON));
        $('#dialog-form').dialog('open');
    });

    $('#iFrame').load(function(){
        $(this).contents().find('#tellParent').click(function(){
            $('#iFrame').contents().find(".cardMini").click(function(){
                $("#iFrame").contents().find(".cardMini").css('background-image', 'url("/MGS/resources/img_trans.gif")');
                $(this).css('background-image', 'url("/MGS/resources/hover.png")' );
                selectedCardId = $(this).data('id');
                selectedCardType = $(this).data('type');
                selectedCardName = $(this).data('name');
                //console.log(selectedCardId);
                console.log(selectedCardType);
            });
        });
    });

    $('#add').click(function(){
        if(selectedCardId != null){
            var indexInArray = $.inArray(selectedCardId, deckIndex);
            if(indexInArray > -1){
                deckJSON.deck[indexInArray].qty ++;
                console.log(deckJSON.deck[indexInArray]);
                $('#land').find('#'+selectedCardId).find('span').html(deckJSON.deck[indexInArray].qty);
                $('#creature').find('#'+selectedCardId).find('span').html(deckJSON.deck[indexInArray].qty);
                $('#spell').find('#'+selectedCardId).find('span').html(deckJSON.deck[indexInArray].qty);
            }else{
                var card = {
                    "id": "",
                    "qty": ""
                };
                card.id = selectedCardId;
                card.qty = 1;
                deckIndex.push(selectedCardId);
                deckJSON.deck.push(card);
                console.log(card);
                if( selectedCardType == 'BASIC' || selectedCardType == 'LAND'){
                    $('#land').append('<dd id="'+selectedCardId+'"></dd>');
                    var selectedCardElement = $('#'+selectedCardId);
                    selectedCardElement.html('<span></span>'+selectedCardName);
                    selectedCardElement.find('span').html('1');
                }else if(selectedCardType == 'CREATURE'){
                    $('#creature').append('<dd id="'+selectedCardId+'"></dd>');
                    var selectedCardElement = $('#'+selectedCardId);
                    selectedCardElement.html('<span></span>'+selectedCardName);
                    selectedCardElement.find('span').html('1');
                }else{
                    $('#spell').append('<dd id="'+selectedCardId+'"></dd>');
                    var selectedCardElement = $('#'+selectedCardId);
                    selectedCardElement.html('<span></span>'+selectedCardName);
                    selectedCardElement.find('span').html('1');
                }
            }

        }
    });

    $('#add-sideboard').click(function(){
        if(selectedCardId != null){
            var indexInArray = $.inArray(selectedCardId, sideboardIndex);
            if(indexInArray > -1){
                deckJSON.sideboard[indexInArray].qty++;
                $('#sideboard').find('#side'+selectedCardId).find('span').html(deckJSON.sideboard[indexInArray].qty);
            }else{
                var card = {
                    "id": "",
                    "qty": ""
                };
                card.id = selectedCardId;
                card.qty = 1;
                sideboardIndex.push(selectedCardId);
                deckJSON.sideboard.push(card);
                console.log(card);
                $('#sideboard').append('<dd id="side'+selectedCardId+'"></dd>');
                var selectedCardElement = $('#side'+selectedCardId);
                selectedCardElement.html('<span></span>'+selectedCardName);
                selectedCardElement.find('span').html('1');
            }
        }
    });
});
