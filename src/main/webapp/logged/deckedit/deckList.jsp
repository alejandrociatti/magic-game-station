<%@ page import="edu.austral.magic.components.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 5/20/13
  Time: 5:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/stylesheet.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/css/smoothness/jquery-ui-other.css" type="text/css"/>

<html>
<head>
    <title></title>
</head>
<%
    request.setAttribute("decks", ((User)session.getAttribute("user")).getDecks());
%>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/deckList.js"></script>
<body>

<div class="shadow">
    <div class="searchFieldFixed">
        <div class="deckList">
            <dl id="decks">
                <%--@elvariable id="decks" type="java.util.List<edu.austral.magic.components.Deck>"--%>
                <c:if test="${decks != null}">
                    <c:forEach items="${decks}" var="deck">
                        <dd id="${deck.deckId}" data-id="${deck.deckId}">${deck.name}</dd>
                    </c:forEach>
                </c:if>
            </dl>
        </div>

        <a id="new-deck" href="${pageContext.request.contextPath}/logged/deckedit/deckEditor.jsp">Create New Deck</a>
    </div>
</div>

<form action="" method="POST">
    <input type="hidden" name="action" value="DECK_EDIT">
    <input type="hidden" id="deck-id" name="deckID">
</form>

</body>
</html>