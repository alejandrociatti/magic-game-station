<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 6/12/13
  Time: 10:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/atmosphere/jquery.atmosphere-min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/atmosphere/messages.js"></script>

<html>
<head>
    <title>Apache Tomcat WebSocket Examples: Chat</title>
    <style type="text/css">
        input#chat {
            width: 410px
        }

        #console-container {
            width: 400px;
        }

        #console {
            border: 1px solid #CCCCCC;
            border-right-color: #999999;
            border-bottom-color: #999999;
            height: 170px;
            overflow-y: scroll;
            padding: 5px;
            width: 100%;
        }

        #console p {
            padding: 0;
            margin: 0;
        }
    </style>
    <script type="text/javascript">


    </script>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websockets rely on Javascript being enabled. Please enable
    Javascript and reload this page!</h2></noscript>
<div>
    <p>
        <input type="text" placeholder="type and press enter to chat" id="chat">
    </p>
    <div id="console-container">
        <div id="console"></div>
    </div>
</div>
</body>
</html>