<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="edu.austral.magic.components.User" %>
<%@ page import="edu.austral.magic.dao.CardDAO" %>
<%@ page import="edu.austral.magic.hibernate.HibernateUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Magic Game Station</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/mc/crafty.css">
    <link rel="canonical" href=""><!-- http://goo.gl/wKFDI -->
    <link rel=icon href="${pageContext.request.contextPath}/resources/mc/favicon.ico">

    <script src="${pageContext.request.contextPath}/resources/mc/js/modernizr.min.js"></script>

    <style>
        .widget-content{height: 200px}
    </style>
</head>
<%
    request.setAttribute("sets", CardDAO.getCardDAO().getSets(HibernateUtil.getGuestSession()));
    try{
        User user = (User)session.getAttribute("user");
        request.setAttribute("isLogged", true);
        request.setAttribute("isAdmin", user.isAdmin());
        request.setAttribute("user", user);
        request.setAttribute("userID", user.getId());
    }catch(NullPointerException e){
        request.setAttribute("isAdmin", false);
        request.setAttribute("isLogged", false);
    }
%>
<script>
    var isAdmin = ${isAdmin};
    var isLogged = ${isLogged};
</script>
<body>
<!--[if IE]><div class="ie"><![endif]-->
<div id="wrapper">
    <header class="clearfix container">
        <hgroup>
            <h1><a href="index.jsp" id="big-header">Magic Game Station</a></h1>
            <h2>Let's play some Magic!</h2>
        </hgroup>
        <nav role="navigation">
            <ul>
                <li><a id='decks-button' href="${pageContext.request.contextPath}/logged/deckedit/deckList.jsp">Decks</a></li>
                <li><a id='find-match-button' href="${pageContext.request.contextPath}/logged/game/roster.jsp">Find a Match</a></li>
                <li><a id='quick-join-button' href="#">Quick Join!</a></li>
                <li><a id='search-button' href="${pageContext.request.contextPath}/newFinder.jsp">Search Cards</a></li>
                <li><a id='log-out-button' href="${pageContext.request.contextPath}">Log Out</a></li>
                <li><a id="admin-button" href="${pageContext.request.contextPath}/logged/admin/adminMenu.jsp">Admin Menu</a></li>
            </ul>
        </nav>
    </header>
    <div class="container">
        <div id="content-before"><!-- Should be empty --></div>
        <aside role="complimentary" style="float: none">

            <div class="topper">
                <div class="small-container" style="float: left">
                    <div class="widget" id="games-widget">
                        <h4 class="widget-title">Game List</h4>
                        <div class="widget-content" id="roster">
                            <ul>
                                <li>
                                    <span>Games</span>
                                    <ul id="waiting-list"></ul>
                                </li>
                            </ul>

                            <button id='refresh'>Refresh Game List</button>
                            <button id='wait'>Wait For Opponent</button>

                        </div><!-- .widget-content -->
                    </div><!-- .widget -->
                </div>

                <div class="small-container" style="margin-left: 355px">
                    <div class="widget" id="confirm-widget">
                        <h4 class="widget-title">Game</h4>
                        <div class="widget-content" id="deck-widget-content">

                        </div><!-- .widget-content -->
                        <form id='confirmation-form' class="hidden" method="POST" action="${pageContext.request.contextPath}/game-manager">
                            <input type="hidden" name="player1" id="player1">
                            <input type="hidden" name="player2" id="player2">
                            <input type="hidden" name="action" value="GAME_START">
                        </form>

                        <form id="join-form" class="hidden" method="POST" action="${pageContext.request.contextPath}/game-manager">
                            <input type="hidden" name="player1" id="jplayer1">
                            <input type="hidden" name="player2" id="jplayer2">
                            <input type="hidden" name="action" value="GAME_STARTED_JOIN">
                        </form>
                    </div><!-- .widget -->
                </div>
            </div>


        </aside><!-- container on center -->
        <div role="main" id="main">

        </div><!-- main -->
        <div id="content-after"><!-- Should be empty --></div>
    </div><!-- .container -->
    <footer>
        <p>This template was created by <a href="http://d3x.co" target="_blank">D3X Solutions</a> and is licensed under <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU GPL 2</a>.</p>
    </footer>
</div><!-- #wrapper -->

<!--[if IE]></div><![endif]-->
<!--Place your JavaScript down here-->
<script src="${pageContext.request.contextPath}/resources/mc/js/fontloader.js"></script><!-- Load custom online fonts http://goo.gl/Deqf0 -->
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/SHA2.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/buttonManager.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/loginScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/logOutScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/finderManager.js"></script>
<script>
    var user = ${userID};
</script>
<script src="${pageContext.request.contextPath}/resources/scripts/gameRosterManager.js"></script>

</body>
</html>


