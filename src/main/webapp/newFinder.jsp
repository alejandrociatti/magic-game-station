<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="edu.austral.magic.components.User" %>
<%@ page import="edu.austral.magic.dao.CardDAO" %>
<%@ page import="edu.austral.magic.hibernate.HibernateUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Magic Game Station</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/mc/crafty.css">
    <link rel="canonical" href=""><!-- http://goo.gl/wKFDI -->
    <link rel=icon href="${pageContext.request.contextPath}/resources/mc/favicon.ico">

    <script src="${pageContext.request.contextPath}/resources/mc/js/modernizr.min.js"></script>
</head>
<%
    request.setAttribute("sets", CardDAO.getCardDAO().getSets(HibernateUtil.getGuestSession()));
    try{
        request.setAttribute("isLogged", true);
        request.setAttribute("isAdmin", ((User)session.getAttribute("user")).isAdmin());
        request.setAttribute("user", session.getAttribute("user"));
    }catch(NullPointerException e){
        request.setAttribute("isAdmin", false);
        request.setAttribute("isLogged", false);
    }
%>
<script>
    var isAdmin = ${isAdmin};
    var isLogged = ${isLogged};
</script>
<body>
<!--[if IE]><div class="ie"><![endif]-->
<div id="wrapper">
    <header class="clearfix container">
        <hgroup>
            <h1><a href="index.jsp" id="big-header">Magic Game Station</a></h1>
            <h2>Let's play some Magic!</h2>
        </hgroup>
        <nav role="navigation">
            <ul>
                <li><a id='decks-button' href="${pageContext.request.contextPath}/logged/deckedit/deckList.jsp">Decks</a></li>
                <li><a id='find-match-button' href="${pageContext.request.contextPath}/logged/game/roster.jsp">Find a Match</a></li>
                <li><a id='quick-join-button' href="#">Quick Join!</a></li>
                <li><a id='search-button' href="${pageContext.request.contextPath}/newFinder.jsp">Search Cards</a></li>
                <li><a id='log-out-button' href="${pageContext.request.contextPath}">Log Out</a></li>
                <li><a id="admin-button" href="${pageContext.request.contextPath}/logged/admin/adminMenu.jsp">Admin Menu</a></li>
            </ul>
        </nav>
    </header>
    <div class="container">
        <aside role="complimentary" id="complimentary">

            <div class="widget" id="login">
                <h4 class="widget-title">Log In</h4>
                <div class="widget-content">
                    <form class="clearfix" method="post" action="${pageContext.request.contextPath}/login" id="widget-login">
                        <p id="widget-login-tips">Password is case-sensitive</p>
                        <label for="login-username">Username</label>
                        <input id="login-username" type="text" name="username" required>
                        <label for="login-password">Password</label>
                        <input id="login-password" type="password" name="password" required>
                        <input type="hidden" name="action" value="USER_LOG_IN">
                        <input type="submit" value="Login" class="loginbtn">
                        <a href="${pageContext.request.contextPath}/signup/signupForm.jsp">Register</a>
                    </form>
                </div><!-- .widget-content -->
            </div><!-- .widget -->

        </aside><!-- complimentary -->
        <div id="content-before"><!-- Should be empty --></div>
        <aside role="complimentary" style="float: none">
            <div class="big-widget" id="signup-widget">
                <h4 class="big-widget-title">Finder</h4>
                <div class="big-widget-content" style="height: 440px">
                    <form id="finder-form">
                        <input id="searchForm" type="text" placeholder="Search Terms..." name="search" style="width:330px">
                        <br>
                        <label><b>Search By:</b></label><br>
                        <input type="radio" name="byWhat" value="name" id="name-radio" checked>
                        <button class='checker' type="button" id="name-button"><label class='white'>[X]</label> Name</button>
                        <input type="radio" name="byWhat" value="type" id="type-radio">
                        <button class='checker' type="button" id="type-button"><label class='white'>[ ]</label> Types</button>
                        <input type="radio" name="byWhat" value="rules" id="text-radio">
                        <button class='checker' type="button" id="text-button"><label class='white'>[ ]</label> Text</button>
                        <div class="searchOptions">
                            <label><b>Filter Card Set: </b></label><br>
                            <select name="set">
                                <option value=""></option>
                                <%--@elvariable id="sets" type="java.util.List<java.lang.String>"--%>
                                <c:if test="${sets != null}">
                                    <c:forEach items="${sets}" var="set">
                                        <option value='${set}'>${set}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                        <div class="searchOptions">
                            <label><b>Filter Card Type: </b></label><br>
                            <select name="type">
                                <option value=""></option>
                                <option value="Artifact">Artifact</option>
                                <option value="Basic">Basic</option>
                                <option value="Creature">Creature</option>
                                <option value="Enchantment">Enchantment</option>
                                <option value="Instant">Instant</option>
                                <option value="Land">Land</option>
                                <option value="Legendary">Legendary</option>
                                <option value="Planeswalker">Planeswalker</option>
                                <option value="Snow">Snow</option>
                                <option value="Sorcery">Sorcery</option>
                                <option value="Tribal">Tribal</option>
                            </select>
                        </div>
                        <div class="filter-div">
                            <label><b>Filter Card Color: </b></label><br>
                            <input type="checkbox" name="whiteMana" id="white-mana">
                            <button class="checker" type="button" id="white-mana-button"><label class='white'>[ ]</label>
                                <img class="whiteMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                            </button>
                            <input type="checkbox" name="blueMana" id="blue-mana">
                            <button class="checker" type="button" id="blue-mana-button"><label class='white'>[ ]</label>
                                <img class="blueMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                            </button>
                            <input type="checkbox" name="blackMana" id="black-mana">
                            <button class="checker" type="button" id="black-mana-button"><label class='white'>[ ]</label>
                                <img class="blackMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                            </button>
                            <input type="checkbox" name="redMana" id="red-mana">
                            <button class="checker" type="button" id="red-mana-button"><label class='white'>[ ]</label>
                                <img class="redMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                            </button>
                            <input type="checkbox" name="greenMana" id="green-mana">
                                <button class="checker" type="button" id="green-mana-button"><label class='white'>[ ]</label>
                                    <img class="greenMana" src="${pageContext.request.contextPath}/resources/img_trans.gif" width="15">
                                </button>
                        </div>
                        <div class="filter-div">
                            <input type="checkbox" name="exact">
                            <button class="checker" type="button" style="width: 381px"><label class='white'>[ ]</label> Match Colors <b>Exactly</b></button>
                            <input type="checkbox" name="exclude">
                            <button class="checker" type="button" style="width: 381px"><label class='white'>[ ]</label> <b>Exclude</b> unselected Colors</button>
                            </input>
                        </div>
                        <input type="hidden" name="action" value="CARD_SEARCH">
                        <input type="submit" value="Search">
                    </form>
                </div><!-- big widget content -->
            </div><!-- big widget -->
            <div class="big-widget" id="results-widget">
                <h4 class="big-widget-title">Results</h4>
                <div class="big-widget-content" id="results-widget-content">
                    <div class="big-widget-content-centered" id="results-container">

                    </div>
                </div><!-- big widget content -->
            </div><!-- big widget -->
        </aside><!-- container on center -->
        <div role="main" id="main">

        </div><!-- main -->
        <div id="content-after"><!-- Should be empty --></div>
    </div><!-- .container -->
    <footer>
        <p>This template was created by <a href="http://d3x.co" target="_blank">D3X Solutions</a> and is licensed under <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU GPL 2</a>.</p>
    </footer>
</div><!-- #wrapper -->

<!--[if IE]></div><![endif]-->
<!--Place your JavaScript down here-->
<script src="${pageContext.request.contextPath}/resources/mc/js/fontloader.js"></script><!-- Load custom online fonts http://goo.gl/Deqf0 -->
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/SHA2.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/buttonManager.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/loginScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/logOutScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/finderManager.js"></script>

</body>
</html>


