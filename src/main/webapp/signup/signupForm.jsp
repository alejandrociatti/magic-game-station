<%@ page import="edu.austral.magic.messages.Messages" %>
<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 4/3/13
  Time: 8:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Magic Game Station</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/mc/crafty.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/css/smoothness/jquery-ui-1.10.3.custom.min.css">
    <link rel="canonical" href=""><!-- http://goo.gl/wKFDI -->
    <link rel=icon href="${pageContext.request.contextPath}/resources/mc/favicon.ico">

    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>
</head>
<%
    request.setAttribute("shortUser", Messages.USER_TOO_SHORT);
    request.setAttribute("usedUser", Messages.USER_USED);
    request.setAttribute("goodUser", Messages.USER_AVAILABLE);
    request.setAttribute("checkingUser", Messages.USER_CHECKING);
    request.setAttribute("pwdNotMatch", Messages.PWD_MATCH);
%>
<script>
    var shortUser = '${shortUser}';
    var usedUser = '${usedUser}';
    var goodUser = '${goodUser}';
    var checkingUser = '${checkingUser}';
    var passwordMatch = '${pwdNotMatch}';
</script>
<body>
<!--[if IE]><div class="ie"><![endif]-->
<div id="wrapper">
    <header class="clearfix container">
        <hgroup>
            <h1><a href="${pageContext.request.contextPath}/index.jsp">Magic Game Station</a></h1>
            <h2>Let's play some Magic!</h2>
        </hgroup>
        <nav role="navigation">
            <ul>
                <li><a id='decks-button' href="${pageContext.request.contextPath}/logged/deckedit/deckList.jsp">Decks</a></li>
                <li><a id='find-match-button' href="#">Find a Match</a></li>
                <li><a id='quick-join-button' href="#">Quick Join!</a></li>
                <li><a id='search-button' href="${pageContext.request.contextPath}/newFinder.jsp">Search Cards</a></li>
                <li><a id='log-out-button' href="${pageContext.request.contextPath}">Log Out</a></li>
                <li><a id="admin-button" href="${pageContext.request.contextPath}/logged/admin/adminMenu.jsp">Admin Menu</a></li>
            </ul>
        </nav>
    </header>
    <div class="container">
        <aside role="complimentary" id="complimentary">

            <div class="widget" id="search">
                <div class="widget-content">
                    <%--TODO make this form where it belongs (Post to the servlet)--%>
                    <form class="clearfix">
                        <input type="search" placeholder="_">
                        <input type="hidden" name="action" value="CARD_SEARCH_QUICK">
                        <input class="searchbtn" type="submit" value="Search">
                    </form>
                </div><!-- .widget-content -->
            </div><!-- .widget -->

            <div class="widget" id="login">
                <h4 class="widget-title">Log In</h4>
                <div class="widget-content">
                    <form class="clearfix" method="post" action="${pageContext.request.contextPath}/login" id="widget-login">
                        <p id="widget-login-tips">Password is case-sensitive</p>
                        <label for="login-username">Username</label>
                        <input id="login-username" type="text" name="username" required>
                        <label for="login-password">Password</label>
                        <input id="login-password" type="password" name="password" required>
                        <input type="hidden" name="action" value="USER_LOG_IN">
                        <input type="submit" value="Login" class="loginbtn">
                        <a href="${pageContext.request.contextPath}/signup/signupForm.jsp">Register</a>
                    </form>
                </div><!-- .widget-content -->
            </div><!-- .widget -->
        </aside><!-- complimentary -->
        <div id="content-before"><!-- Should be empty --></div>
        <aside role="complimentary" style="float: none">
            <div class="big-widget" id="signup-widget">
                <h4 class="big-widget-title">Sign Up</h4>
                <div class="big-widget-content">
                    <form class="clearfix" action="${pageContext.request.contextPath}/signUp" method="post" id="signUp">
                        <p class=validateTips id="tips">All form fields are required</p>
                        <label for="firstname">First name:</label><br>
                        <input type="text" name="firstname" id="firstname" required><br>
                        <label for="lastname">Last name:</label><br>
                        <input type="text" name="lastname" id="lastname" required><br>
                        <label for="username">User name</label><br>
                        <input type="text" name="username" id="username" required><br>
                        <label for="pwd">Password:</label><br>
                        <input type="password" name="password" id="pwd" required><br>
                        <label for="pwdConfirm">Confirm Password:</label><br>
                        <input type="password" id="pwdConfirm" name="pwdConfirm" required><br>
                        <input type="hidden" name="action" value="USER_ADD">
                        <input type="submit" value="Sign Up" class="commonButton" id="submitForm">
                    </form>
                </div><!-- big widget content -->
            </div><!-- big widget -->
        </aside><!-- container on center -->
        <div id="content-after"><!-- Should be empty --></div>
    </div><!-- .container -->
    <footer>
        <p>This template was created by <a href="http://d3x.co" target="_blank">D3X Solutions</a> and is licensed under <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU GPL 2</a>.</p>
    </footer>
</div><!-- #wrapper -->

<!--[if IE]></div><![endif]-->
<!--Place your JavaScript down here-->
<script src="${pageContext.request.contextPath}/resources/mc/js/fontloader.js"></script><!-- Load custom online fonts http://goo.gl/Deqf0 -->
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/SHA2.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/loginScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/buttonManager.js"></script>
<script src="${pageContext.request.contextPath}/signup/signupValidator.js"></script>


</body>
</html>