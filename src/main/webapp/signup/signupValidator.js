/**
 * Created with IntelliJ IDEA.
 * User: alejandro
 * Date: 5/14/13
 * Time: 7:09 PM
 */

$(document).ready(function(){
    var signUpForm = $('#signUp');
    var tips = $('#tips');
    var usernameAltered = false;
    var firstNameField = $('#firstname');
    var lastNameField = $('#lastname');
    var usernameField = $('#username');
    var passwordField = $('#pwd');
    var pwdConfirmField = $('#pwdConfirm');

    //Function used to update messages in submission dialog
    function updateTips(t) {
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
    }

    //Checks if o.val() complies with the regular expression given
    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass( "ui-state-error" );
            updateTips( n );
            return false;
        } else {
            return true;
        }
    }

    //Checks if o.val().length is within min & max range
    function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " +
                min + " and " + max + "." );
            return false;
        } else {
            return true;
        }
    }

    //Changing username manually disables username suggestion
    usernameField.keyup(function(){
        if(!usernameAltered){
            usernameAltered = true;
        }
    });


    //Username availability check
    function checkUsername(){
        var user = $('#username').val();
        var isValid = false;
        if(user.length > 4){
            $.ajax({
                async: false,
                url: "/MGS/user",
                data: {action: "USER_CHECK", username: user},
                success: function(data){
                    if(data == "true"){
                        updateTips(usedUser);
                        isValid = false;
                    }else{
                        updateTips(" ");
                        isValid = true;
                    }
                }
            });
        }else{
            updateTips(shortUser);
            isValid = false;
        }
        return isValid;
    }

    usernameField.focusout(function(){
        checkUsername();
    });

    //Password consistency check
    function checkPassword(){
        var password = $('#pwdConfirm').val();
        if(password != $('#pwd').val()){
            updateTips(passwordMatch);
            return false;
        }else{
            updateTips(" ");
            return true;
        }
    }

    pwdConfirmField.keyup(function(){
        checkPassword();
    });

    //Username 'prediction'
    lastNameField.keyup(function(){
        if(!usernameAltered){
            var predict = $('#firstname').val();
            predict = predict.substr(0, 1);
            predict = predict + $(this).val();
            $('#username').val(predict);
        }
    });

    //Form validation before submission
    signUpForm.submit(function(event){
        var isValid = true;
        isValid = isValid && checkRegexp(firstNameField, /[a-zA-Z]/i, "First name can consist of A-Z a-z letters only");
        isValid = isValid && checkLength(firstNameField,"first name", 1, 20);
        isValid = isValid && checkRegexp(lastNameField, /[a-zA-Z]/i, "Last name can consist of A-Z a-z letters only");
        isValid = isValid && checkLength(lastNameField, "last name", 1, 20);
        var passwordMessage = "Password MUST have at least one lowercase one uppercase and a number, and be between 4-20 characters long";
        isValid = isValid && checkRegexp(passwordField, /^.*(?=.{4,20})(?=.*\d)(?=.*[a-zA-Z]).*$/i, passwordMessage);
        isValid = isValid && checkUsername();
        isValid = isValid && checkPassword();
        var password = Sha256.hash(passwordField.val());
        console.log(password);
        passwordField.val(password);
        console.log(passwordField.val());
        if(!isValid){
            event.preventDefault();
        }
    });

});
