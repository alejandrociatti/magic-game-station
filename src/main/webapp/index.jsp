<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="edu.austral.magic.components.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Magic Game Station</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/mc/crafty.css">
    <link rel="canonical" href=""><!-- http://goo.gl/wKFDI -->
    <link rel=icon href="${pageContext.request.contextPath}/resources/mc/favicon.ico">

    <script src="${pageContext.request.contextPath}/resources/mc/js/modernizr.min.js"></script>
</head>
<%
    if(request.getAttribute("badAccess") == null){
        request.setAttribute("badAccess", false);
        request.setAttribute("badAccessMessage", "empty");
    }
    try{
        request.setAttribute("isLogged", true);
        request.setAttribute("isAdmin", ((User)session.getAttribute("user")).isAdmin());
        request.setAttribute("username", ((User)session.getAttribute("user")).getUsername());
    }catch(NullPointerException e){
        request.setAttribute("isAdmin", false);
        request.setAttribute("isLogged", false);
    }
%>
<script>
    var isAdmin = ${isAdmin};
    var isLogged = ${isLogged};
    var badAccess = ${badAccess};
    var badAccessMessage = "${badAccessMessage}";
</script>
<body>
<!--[if IE]><div class="ie"><![endif]-->
<div id="wrapper">
    <header class="clearfix container">
        <hgroup>
            <h1><a href="index.jsp" id="big-header">Magic Game Station</a></h1>
            <h2>Let's play some Magic!</h2>
        </hgroup>
        <nav role="navigation">
            <ul>
                <li><a id='decks-button' href="${pageContext.request.contextPath}/logged/deckedit/deckList.jsp">Decks</a></li>
                <li><a id='find-match-button' href="${pageContext.request.contextPath}/logged/game/roster.jsp">Find a Match</a></li>
                <li><a id='quick-join-button' href="#">Quick Join!</a></li>
                <li><a id='search-button' href="${pageContext.request.contextPath}/newFinder.jsp">Search Cards</a></li>
                <li><a id='log-out-button' href="${pageContext.request.contextPath}">Log Out</a></li>
                <li><a id="admin-button" href="${pageContext.request.contextPath}/logged/admin/adminMenu.jsp">Admin Menu</a></li>
            </ul>
        </nav>
    </header>
    <div class="container">
        <aside role="complimentary" id="complimentary">
            <div class="widget" id="search">
                <div class="widget-content">
                    <%--TODO make this form where it belongs (Post to the servlet)--%>
                    <form class="clearfix">
                        <input type="search" placeholder="_">
                        <input type="hidden" name="action" value="CARD_SEARCH_QUICK">
                        <input class="searchbtn" type="submit" value="Search">
                    </form>
                </div><!-- .widget-content -->
            </div><!-- .widget -->
            <div class="widget" id="login">
                <h4 class="widget-title">Log In</h4>
                <div class="widget-content">
                    <form class="clearfix" method="post" action="${pageContext.request.contextPath}/login" id="widget-login">
                        <p id="widget-login-tips">Password is case-sensitive</p>
                        <label for="login-username">Username</label>
                        <input id="login-username" type="text" name="username" required>
                        <label for="login-password">Password</label>
                        <input id="login-password" type="password" name="password" required>
                        <input type="hidden" name="action" value="USER_LOG_IN">
                        <input type="submit" value="Login" class="loginbtn">
                        <a href="${pageContext.request.contextPath}/signup/signupForm.jsp">Register</a>
                    </form>
                </div><!-- .widget-content -->
            </div><!-- .widget -->
            <div class="widget">
                <h4 class="widget-title">Example Widget</h4><!-- The Title not only tops the rounded corners of this widget, but rounds off the bottom of the last one, make sure this is here -->
                <div class="widget-content">
                    <p>This could be anything</p>
                </div><!-- .widget-content -->
            </div><!-- .widget -->
            <div class="widget">
                <h4 class="widget-title">Example Menu</h4>
                <div class="widget-content">
                    <ul>
                        <li><a href="#">Link 01</a></li>
                        <li><a href="#">Another one</a></li>
                        <li><a href="#">The third</a>
                            <ul>
                                <li><a href="#">Sub Item</a></li>
                                <li><a href="#">Sub Item</a></li>
                                <li><a href="#">Sub Item</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Lastly</a></li>
                    </ul>
                </div><!-- .widget-content -->
            </div><!-- .widget -->
            <div class="widget" id="news">
                <h4 class="widget-title">Recent News</h4>
                <div class="widget-content">
                    <ul>
                        <li>
                            <a href="#" class="item-title">This could be some news</a>
                            <div class="item-pubdate">1 June 2013</div>
                        </li>
                        <li>
                            <a href="#" class="item-title">This could be other news</a>
                            <div class="item-pubdate">3 June 2013</div>
                        </li>
                    </ul>
                </div><!-- .widget-content -->
            </div><!-- .widget -->
        </aside><!-- complimentary -->
        <div id="content-before"><!-- Should be empty --></div>
        <div role="main" id="main">
            <article>
                <c:choose>
                    <c:when test="{username != null}">
                        <h1>HELLO <c:out value="${username}"/></h1>
                    </c:when>
                    <c:otherwise>
                        <h1>HELLO GUEST</h1>
                    </c:otherwise>
                </c:choose>
                <h2>Welcome to MGS</h2>
                <%--<a href="http://dstats.net/download/http://design.d3x.co/download/mcgeneric.zip" class="button">Download v1.02 - 09JUL12 (ZIP)</a>--%>
                <h4>Block Quote</h4>
                <blockquote>The problem with posting quotes online is that you can never be sure who wrote them<br>-Abraham Lincoln</blockquote>
                <h4>Sample Text</h4>
                <pre>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rutrum, purus vel faucibus auctor, sem odio tempus augue, id euismod orci odio commodo odio. Integer dictum sodales sodales. Aenean at felis magna. Nullam nibh nisi, ultrices in placerat vel, vulputate non ligula. Cras mattis, elit ac eleifend egestas, mi arcu sodales dui, sit amet elementum lorem lorem a mi. Proin odio nisi, tincidunt vel iaculis eget, tempor eu elit. Quisque adipiscing interdum ligula in viverra. Donec lobortis nibh ante. Quisque vitae adipiscing augue.</pre>
            </article>

        </div><!-- main -->
        <div id="content-after"><!-- Should be empty --></div>
    </div><!-- .container -->
    <footer>
        <p>This template was created by <a href="http://d3x.co" target="_blank">D3X Solutions</a> and is licensed under <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU GPL 2</a>.</p>
    </footer>
</div><!-- #wrapper -->

<!--[if IE]></div><![endif]-->
<!--Place your JavaScript down here-->
<script src="${pageContext.request.contextPath}/resources/mc/js/fontloader.js"></script><!-- Load custom online fonts http://goo.gl/Deqf0 -->
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/jQueryUI/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/SHA2.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/indexAlerts.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/buttonManager.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/loginScript.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/logOutScript.js"></script>

</body>
</html>


