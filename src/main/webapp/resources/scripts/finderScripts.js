$(document).ready(function(){

    $(document).ajaxComplete(function(){
        $('#tellParent').click();
        var cardSelector = $('.cardMini');
        cardSelector.click(function(){
            cardSelector.animate({height:'25px'});
            cardSelector.find('.cardDisplay').css("display","none");
            $(this).animate({height:'366px'});
            $(this).find('.cardDisplay').css("display","block");
        });
    });

    var resultsDiv = $('#results');

    $('#submit').click(function(){
        resultsDiv.empty();
        var data = $('#search').serialize();
        $.get("/MGS/CardManager", data, function(response){
            $.each(response.results, function(index, value){
                var result = $("<div class='cardMini'><div class='cardDisplay'><img class='cardImage'></div></div>");
                result.attr('data-id', value.id);
                result.attr('data-name', value.name);
                result.attr('data-type', value.type);
                result.prepend(value.name);
                var cardImageUrl = "/MGS/resources/"+value.magicSet+"/"+value.name+".full.jpg";
                result.find('.cardImage').attr('src', cardImageUrl);
                resultsDiv.append(result);
            });
//            cardShow();
        }, "json");
    });

});