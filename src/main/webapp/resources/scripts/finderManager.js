$(document).ready(function(){
    var searchForm = $('#finder-form');
    var resultDisplay = $('#results-container');

    //Animates the results in the 'results' widget after they are displayed via AJAX
    $(document).ajaxComplete(function(){
        $('.cardDisplay').hide("blind");
        var cardSelector = $('.cardMini');
        cardSelector.click(function(){
            $('.cardDisplay').hide("blind");
            $(this).next('.cardDisplay').show("blind");
        });
    });

    //Gets the results of the query in the form and displays them in the 'results' widget
    searchForm.submit(function(event){
        event.preventDefault();
        resultDisplay.empty();
        var data = searchForm.serialize();
        $.get("/MGS/CardManager", data, function(response){
            $.each(response.results, function(index, value){
                var result = $("<div><button class='cardMini'/><div class='cardDisplay'><img class='cardImage'></div></div>");
                var resultMini = result.find('.cardMini');
                resultMini.attr('data-id', value.id);
                resultMini.attr('data-name', value.name);
                resultMini.attr('data-type', value.type);
                resultMini.prepend(value.name);
                var cardImageUrl = "/MGS/resources/"+value.magicSet+"/"+value.name+".full.jpg";
                result.find('.cardImage').attr('src', cardImageUrl);
                resultDisplay.append(result);
            });
        }, "json");
    });

    //Form overrides (consequence of pretty buttons)
    //Search By:
    var searchByName = $('#name-radio');
    var searchByType = $('#type-radio');
    var searchByText = $('#text-radio');
    var searchByNameButton = $('#name-button');
    var searchByTypeButton = $('#type-button');
    var searchByTextButton = $('#text-button');

    searchByNameButton.click(function(){
        searchByName.prop('checked', true);
        searchByNameButton.html("[X] Name");
        searchByTypeButton.html("[ ] Types");
        searchByTextButton.html("[ ] Text");
    });

    searchByTypeButton.click(function(){
        searchByType.prop('checked', true);
        searchByNameButton.html("[ ] Name");
        searchByTypeButton.html("[X] Types");
        searchByTextButton.html("[ ] Text");
    });

    searchByTextButton.click(function(){
        searchByText.prop('checked', true);
        searchByNameButton.html("[ ] Name");
        searchByTypeButton.html("[ ] Types");
        searchByTextButton.html("[X] Text");
    });

    //Filter Card Colors:
    var colorChooserButton = $('.filter-div').find('.checker');
    //Set all buttons to unchecked
    colorChooserButton.data('state', false);
    colorChooserButton.click(function(){
        //invert state
        var invertedState = !$(this).data('state');
        console.log('state of button '+$(this)+'is '+invertedState);
        console.log(typeof invertedState);
        $(this).data('state', invertedState);
        //set checkbox to that inverted state
        $(this).prev().prop('checked', invertedState);
        //change button appearance to reflect state
        if(invertedState){
            $(this).find('label').html('[X]');
        }else{
            $(this).find('label').html('[ ]');
        }
    });
});