$(document).ready(function(){

    function refreshList(){
        list.html(' ');
        $.get(gameManagerURL, {action: "GAME_WAITING"}, function(success){
            $.each(success, function(index, value){
                var toAdd = $("<li></li>");
                toAdd.prepend(value.username);
                toAdd.attr('data-id', value.id);
                list.prepend(toAdd);
            });
        });
    }

    var gameManagerURL = "/MGS/game-manager";
    var list = $('#waiting-list');
    var widget = $('#deck-widget-content');
    var confirmationForm = $('#confirmation-form');
    var joiningForm = $('#join-form');
    refreshList();

    $('#wait').click(function(){
        list.html(' ');
        $.get(gameManagerURL, {action: "GAME_WAIT", user: user}, function(success){
            $.each(success, function(index, value){
                var toAdd = $("<li></li>");
                toAdd.prepend(value.username);
                toAdd.attr('data-id', value.id);
                list.prepend(toAdd);
            });

            setInterval(function(){
                $.get(gameManagerURL, {action: "GAME_AM_I_ALONE", user: user}, function(success){
                    if(success.joiningID != null){
                        var joiningPlayer = $('<label></label>');
                        joiningPlayer.prepend(success.joiningUsername);
                        joiningPlayer.attr("data-id", joiningID);
                        widget.html(joiningPlayer);
                        var button = $('<br><button id="confirm">Start Game</button>');
                        widget.prepend(button);
                        button = $('<br><button id="cancel">Cancel</button>');
                        widget.prepend(button);

                        //On Confirm
                        widget.find('#confirm').click(function(){
                            $.get(gameManagerURL, {action: 'GAME_CONFIRM', user: user}, function(success){
                                widget.html('<label>Creating game, please wait 15 seconds</label>');
                                var opID = success.opID;
                                setTimeout(function(){
                                    joiningForm.find('#jplayer1').attr("value", user);
                                    joiningForm.find('#jplayer2').attr("value", opID);
                                    joiningForm.submit();
                                }, 15000);
                            });
                        });

                        //On cancel
                        widget.find('#cancel').click(function(){
                            $.get(gameManagerURL, {action: 'GAME_WAIT_CANCEL', user: user}, function(success){
                                widget.html(' ');
                            });
                        });
                    }
                });
            }, 5000);
        });
    });

    $('#refresh').click(function(){
        refreshList();
    });

    list.find('li').click(function(){
        $(this).css('background-color', 'gray');
        var opID = $(this).data('id');
        $.get(gameManagerURL, {action: "GAME_JOIN", user: user, join: opID}, function(success){
            widget.html('<label>Waiting for opponent to confirm</label><br><button id="cancel">Cancel</button>');
            widget.find('#cancel').click(function(){
                $.get(gameManagerURL, {action: 'GAME_JOIN_CANCEL', user: user}, function(success){
                    widget.html(' ')
                });
            });

            setInterval(function(){
                $.get(gameManagerURL, {action: 'GAME_AM_I_CONFIRMED', user: user}, function(success){
                    if(success.confirmed){
                        confirmationForm.find('#player1').attr('value', success.opID);
                        confirmationForm.find('#player2').attr('value', user);
                        confirmationForm.submit();
                    }
                });
            }, 5000);
        });
    });





});