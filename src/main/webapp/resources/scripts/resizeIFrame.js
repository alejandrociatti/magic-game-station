/**
 * Created with IntelliJ IDEA.
 * User: Alejandro
 * Date: 13/05/13
 * Time: 05:01
 * To change this template use File | Settings | File Templates.
 */


$(document).ready(function(){
    function resize(){
        var changeHeight = document.getElementById('iFrame').contentWindow.document.body.offsetHeight+366;
        $('#iFrame').css('height', changeHeight);
    }

    $('iFrame').load(function(){
        $('#iFrame').contents().find('#tellParent').click(function(){
            resize();
        });
    });
});
