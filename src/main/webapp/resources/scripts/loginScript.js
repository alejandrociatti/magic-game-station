$(document).ready(function(){
    var logInForm = $('#widget-login');
    var tips = $('#widget-login-tips');
    var userName = $('#login-username');
    var password = $('#login-password');

    //Function used to update messages in submission dialog
    function updateTips(t) {
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
    }

    //Checks if o.val().length is within min & max range
    function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " +
                min + " and " + max + "." );
            return false;
        } else {
            return true;
        }
    }

    //Checks if o.val() complies with the regular expression given
    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass( "ui-state-error" );
            updateTips( n );
            return false;
        } else {
            return true;
        }
    }

    logInForm.submit(function(event){
        var isValid = true;
        isValid = isValid && checkLength(userName, "user name", 4, 20);
        isValid = isValid && checkRegexp(userName, /[a-z0-9]/i, "username characters may only consist of lowercase letters & numbers");
        isValid = isValid && checkRegexp(password, /^.*(?=.{4,20})(?=.*\d)(?=.*[a-zA-Z]).*$/i, "that can't possibly be your password");

        if(isValid){
            var hashedPwd = Sha256.hash(password.val(), false);
            password.val(hashedPwd);
            return true;
        }else{
            event.preventDefault();
        }
    });
});