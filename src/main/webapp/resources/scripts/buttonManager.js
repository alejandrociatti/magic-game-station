$(document).ready(function(){
    var decksButton = $('#decks-button');
    var findMatchButton = $('#find-match-button');
    var quickJoinButton = $('#quick-join-button');
    var logOutButton = $('#log-out-button');
    var adminButton = $('#admin-button');

    decksButton.prop('disabled', true);
    findMatchButton.prop('disabled', true);
    quickJoinButton.prop('disabled', true);
    adminButton.hide();
    logOutButton.hide();

    if(isAdmin){
        adminButton.show();
    }

    if(isLogged){
        $('#login').hide();
        logOutButton.show();
        decksButton.prop('disabled', false);
        findMatchButton.prop('disabled', false);
        quickJoinButton.prop('disabled', false);
    }
});