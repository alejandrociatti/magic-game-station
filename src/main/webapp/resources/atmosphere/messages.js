var Game = {};

Game.socket = null;

Game.connect = (function(host) {
    if ('WebSocket' in window) {
        Game.socket = new WebSocket(host);
    } else if ('MozWebSocket' in window) {
        Game.socket = new MozWebSocket(host);
    } else {
        Console.log('Error: WebSocket is not supported by this browser.');
        return;
    }

    Game.socket.onopen = function () {
        Console.log('Info: WebSocket connection opened.');
        document.getElementById('chat').onkeydown = function(event) {
            if (event.keyCode == 13) {
                Game.sendMessage();
            }
        };
    };

    Game.socket.onclose = function () {
        document.getElementById('chat').onkeydown = null;
        Console.log('Info: WebSocket closed.');
    };

    Game.socket.onmessage = function (message) {
        Console.log(message.data);
    };
});

Game.initialize = function() {
    if (window.location.protocol == 'http:') {
        Game.connect("ws://"+document.location.host+"/MGS/game");
    } else {
        Game.connect("wss://"+document.location.host+"/MGS/game");
    }
};

Game.sendMessage = (function() {
    var message = document.getElementById('chat').value;
    if (message != '') {
        Game.socket.send(message);
        document.getElementById('chat').value = '';
    }
});

var Console = {};

Console.log = (function(message) {
    var console = document.getElementById('console');
    var p = document.createElement('p');
    p.style.wordWrap = 'break-word';
    p.innerHTML = message;
    console.appendChild(p);
    while (console.childNodes.length > 25) {
        console.removeChild(console.firstChild);
    }
    console.scrollTop = console.scrollHeight;
});

Game.initialize();