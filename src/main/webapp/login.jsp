<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 4/17/13
  Time: 12:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/stylesheet.css" type="text/css"/>
<html>
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>
<body>
    <div class="shadow">
        <div class="searchFieldFixed">
            <form action="/MGS/login" method="post">
                <input type="text" placeholder="username" name="user"><br>
                <input type="password" placeholder="password" name="password"><br>
                <input type="hidden" name="action" value="USER_LOG_IN">
                <input type="submit" value="Log In" class="commonButton">
            </form>
            <form action="${pageContext.request.contextPath}/signUp" method="post">
                <input type="hidden" name="action" value="USER_SIGN_UP">
                <input type="submit" name="action" value="Or Sign Up!" class="commonButton">
            </form>
        </div>
    </div>
</body>
</html>