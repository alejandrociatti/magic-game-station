package edu.austral.magic.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * User: alejandro
 * Date: 4/10/13
 * Time: 12:21 PM
 */
public class HibernateUtil {
    private static HibernateUtil ourInstance = new HibernateUtil();
    private final SessionFactory sessionFactory;
    private final Session guestSession;

    private HibernateUtil() {
        try{
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
                    .buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            guestSession = sessionFactory.openSession();
        }catch(Throwable ex){
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return ourInstance.sessionFactory;
    }

    public static Session getGuestSession(){
        return ourInstance.guestSession;
    }
}
