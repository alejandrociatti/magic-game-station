package edu.austral.magic.servlet;

/**
 * User: alejandro
 * Date: 4/10/13
 * Time: 9:47 AM
 */
public enum ActionID {
    //Card Actions
    CARD_ADD, CARD_DELETE, CARD_MODIFY, CARD_SEARCH, CARD_GET,
    //Deck Actions
    DECK_CREATE, DECK_EDIT,
    //User Actions
    USER_ADD, USER_CHECK, USER_LOG_IN, USER_LOG_OUT, USER_SIGN_UP,
    //Game Actions
    GAME_WAIT, GAME_WAITING, GAME_JOIN, GAME_JOIN_CANCEL, GAME_AM_I_ALONE, GAME_AM_I_CONFIRMED, GAME_START,
    GAME_STARTED_JOIN ,GAME_WAIT_CANCEL, GAME_CONFIRM,
    //Redirection Actions
    REDIRECT_SEARCH,
    //Unknown (default) action
    UNKNOWN,
}
