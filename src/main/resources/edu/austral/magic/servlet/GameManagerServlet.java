package edu.austral.magic.servlet;

import edu.austral.magic.components.User;
import edu.austral.magic.dao.UserDAO;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Game Manager", urlPatterns = {"/game-manager"})
public class GameManagerServlet extends ActionHandlerServlet {
    List<Long> waitingPlayers = new ArrayList<Long>();
    List<Long> joiningPlayers = new ArrayList<Long>();
    List<Integer> confirmedGames = new ArrayList<Integer>();


    @Override
    protected void handleActionGet(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws IOException {
        switch (actionID){
            case GAME_WAIT:
                try {
                    waitForOpponent(req, resp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            case GAME_WAIT_CANCEL: cancelWaitingGame(req, resp);
            case GAME_WAITING: getWaitingPlayers(req, resp);
            case GAME_JOIN: joinGame(req, resp);
            case GAME_CONFIRM: confirmGame(req, resp);
            case GAME_JOIN_CANCEL: joinGameCancel(req, resp);
            case GAME_AM_I_ALONE: checkForJoining(req, resp);
            case GAME_AM_I_CONFIRMED: checkIfConfirmed(req, resp);
            case UNKNOWN: System.err.println("GET request with UNKNOWN action performed");
        }
    }

    @Override
    protected String handleActionPost(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws ServletException, IOException {
        switch (actionID){
            case GAME_START: return createAndStartGame(req, resp);
            case GAME_STARTED_JOIN: return joinStartedGame(req, resp);
            case UNKNOWN: return "/index.jsp";
            default: return "/index.jsp";
        }
    }

    private void confirmGame(HttpServletRequest req, HttpServletResponse resp) {
        int index = waitingPlayers.indexOf(Long.valueOf(req.getParameter("user")));
        confirmedGames.add(index);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        JSONObject jsonToWrite = new JSONObject();
        try {
            jsonToWrite.put("opID", joiningPlayers.get(index));
            resp.getWriter().write(jsonToWrite.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //GET action handlers
    private void waitForOpponent(HttpServletRequest req, HttpServletResponse resp) throws JSONException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        long userID = Long.valueOf(req.getParameter("user"));
        //Check if player is already waiting, so they only play one game at a time
        if(!waitingPlayers.contains(userID)){
            //If he isn't add him to the roster
            waitingPlayers.add(userID);
            //Get Hibernate Session from User to associate userID to usernames
            Session session = ((User)req.getSession().getAttribute("user")).getSession();
            resp.getWriter().write(getWaitingPlayers(session));
        }else{
            JSONArray alert = new JSONArray();
            JSONObject user = new JSONObject();
            user.put("username", "one game at a time");
            user.put("id", -1);
            alert.put(user);
            resp.getWriter().write(alert.toString());
        }
    }

    private void getWaitingPlayers(HttpServletRequest req, HttpServletResponse resp) {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        Session session = ((User)req.getSession().getAttribute("user")).getSession();
        try {
            resp.getWriter().write(getWaitingPlayers(session));
        } catch (IOException e) {
            System.err.println("Could not get response writer");
            e.printStackTrace();
        }
    }

    private String getWaitingPlayers(Session session){
        try{
            JSONObject[] waitingPlayersUsername = new JSONObject[waitingPlayers.size()];
            int i = 0;
            for (Long waitingPlayer : waitingPlayers) {
                JSONObject user = new JSONObject();
                user.put("username", UserDAO.getInstance().getUsername(session, waitingPlayer));
                user.put("id", waitingPlayer);
                waitingPlayersUsername[i] = user;
                i++;
            }
            JSONArray waitingPlayersJSON = new JSONArray(waitingPlayersUsername);
            return waitingPlayersJSON.toString();
        }catch(JSONException e){
            e.printStackTrace();
            return null;
        }
    }

    private void joinGame(HttpServletRequest req, HttpServletResponse resp) {
        long joinID = Long.valueOf(req.getParameter("join"));
        if(waitingPlayers.contains(joinID)){
            joiningPlayers.add(waitingPlayers.indexOf(joinID), joinID);
        }
    }

    private void joinGameCancel(HttpServletRequest req, HttpServletResponse resp){
        long joiningID = Long.valueOf(req.getParameter("user"));
        if(joiningPlayers.contains(joiningID)){
            joiningPlayers.remove(joiningID);
        }
    }

    private void checkForJoining(HttpServletRequest req, HttpServletResponse resp) {
        long waitingID = Long.valueOf(req.getParameter("user"));
            if(waitingPlayers.contains(waitingID)){
                if(joiningPlayers.get(waitingPlayers.indexOf(waitingID)) != null){
                    resp.setCharacterEncoding("UTF-8");
                    resp.setContentType("application/json");
                    JSONObject jsonToWrite = new JSONObject();
                    Session session = ((User)req.getSession().getAttribute("user")).getSession();
                    long joiningID = joiningPlayers.get(waitingPlayers.indexOf(waitingID));
                    try {
                        jsonToWrite.put("joiningID", joiningID);
                        jsonToWrite.put("joiningUsername", UserDAO.getInstance().getUsername(session, joiningID));
                        resp.getWriter().write(jsonToWrite.toString());
                    } catch (JSONException e) {
                        System.err.println("Failed to create JSON of player joining a game");
                        e.printStackTrace();
                    } catch (IOException e) {
                        System.err.println("Could NOT write joining player JSON to the response");
                        e.printStackTrace();
                    }
                }
            }
    }

    private void cancelWaitingGame(HttpServletRequest req, HttpServletResponse resp) {
        long userID = Long.valueOf(req.getParameter("user"));
        int index = waitingPlayers.indexOf(userID);
        if(joiningPlayers.get(index) != null){
            joiningPlayers.remove(index);
        }
        waitingPlayers.remove(index);
    }

    private void checkIfConfirmed(HttpServletRequest req, HttpServletResponse resp) {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json");
        long joiningID = Long.valueOf(req.getParameter("user"));
        int index = joiningPlayers.indexOf(joiningID);
        Session session = ((User)req.getSession().getAttribute("user")).getSession();
        if(confirmedGames.contains(index)){
            JSONObject jsonToWrite = new JSONObject();
            try {
                jsonToWrite.put("confirmed", true);
                long opponent = waitingPlayers.get(index);
                jsonToWrite.put("opID", opponent);
                jsonToWrite.put("opUsername", UserDAO.getInstance().getUsername(session, opponent));
                resp.getWriter().write(jsonToWrite.toString());
            } catch (JSONException e) {
                System.err.println("Could NOT create JSON to confirm game creation/start");
                e.printStackTrace();
            } catch (IOException e) {
                System.err.println("Could NOT write game confirmation JSON to the response");
                e.printStackTrace();
            }
        }
    }

    //POST action handlers
    private String createAndStartGame(HttpServletRequest req, HttpServletResponse resp) {
        return "/logged/game/play.jsp";
    }

    private String joinStartedGame(HttpServletRequest req, HttpServletResponse resp) {


        return "/logged/game/play.jsp";
    }
}
