package edu.austral.magic.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: alejandro
 * Date: 4/5/13
 * Time: 10:27 AM
 */

public abstract class ActionHandlerServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req,
                      HttpServletResponse resp)
            throws ServletException,IOException {
        final String actionIdStr = req.getParameter("action");
        ActionID actionID;
        try {
            actionID = ActionID.valueOf(actionIdStr);
        }catch (IllegalArgumentException e)
        {
            actionID = ActionID.UNKNOWN;
            System.out.println("Unknown Action");
        }
        handleActionGet(req, resp, actionID);
    }

    /**
     * This method handles actions using the parameter "action" from the get request
     */
    protected abstract void handleActionGet(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws IOException;

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException{
        final String actionIdStr = req.getParameter("action");
        ActionID actionID;
        try {
            actionID = ActionID.valueOf(actionIdStr);
        }catch (IllegalArgumentException e)
        {
            actionID = ActionID.UNKNOWN;
            System.out.println("Unknown Action");
        }
        final String jspPage = handleActionPost(req, resp, actionID);
        forward(req, resp, jspPage);
    }

    /**
     * This method handles actions using the parameter "action" from the post request
     *
     * @return returns the url String according to the "action" to be forwarded to
     */
    protected abstract String handleActionPost(HttpServletRequest request,
                                               HttpServletResponse response, ActionID actionID) throws ServletException, IOException;





    private void forward(HttpServletRequest req,
                         HttpServletResponse res, String url) throws ServletException, IOException {
        req.getRequestDispatcher(url).forward(req, res);
    }


    protected void showError(HttpServletRequest req,
                             HttpServletResponse res, Throwable e) throws ServletException, IOException {
        req.setAttribute("error", e);
        forward(req, res, AttributeName.ERROR.toString());
    }
}
