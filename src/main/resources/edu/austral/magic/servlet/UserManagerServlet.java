package edu.austral.magic.servlet;

import edu.austral.magic.components.User;
import edu.austral.magic.dao.UserDAO;
import edu.austral.magic.hibernate.HibernateUtil;
import edu.austral.magic.messages.Messages;
import org.hibernate.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alejandro
 * Date: 4/3/13
 * Time: 8:09 PM
 * To change this template use File | Settings | File Templates.
 */

@WebServlet(name = "User Management", urlPatterns = {"/signUp", "/login", "/logout", "/user"})
public class UserManagerServlet extends ActionHandlerServlet{

    @Override
    protected void handleActionGet(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws IOException {
        switch (actionID){
            case USER_CHECK: checkUser(req, resp);
            case USER_LOG_OUT: logOut(req, resp);
        }
    }

    @Override
    protected String handleActionPost(HttpServletRequest req, HttpServletResponse res, ActionID actionID)
            throws ServletException, IOException {
        //checks request for desired action
        switch(actionID){
            case USER_ADD: return addUser(req, res);
            case USER_LOG_IN: return logIn(req, res);
            case USER_SIGN_UP: return signUp(req, res);
            case USER_LOG_OUT: return logOut(req, res);
            default: return null;
        }
    }

    //GET Action handlers
    private void checkUser(HttpServletRequest req, HttpServletResponse res) throws IOException{
        res.setContentType("text/plain");
        res.setCharacterEncoding("UTF-8");
        List<String> list = UserDAO.getInstance().getUsers(HibernateUtil.getGuestSession());
        //Writes to the response whether or not the username is taken (true for taken)
        res.getWriter().write(Boolean.toString(list.contains(req.getParameter("username"))));
    }

    private String logOut(HttpServletRequest req, HttpServletResponse res) {
        req.getSession().removeAttribute("user");
        return "/index.jsp";
    }

    //POST Action handlers
    private String addUser(HttpServletRequest req, HttpServletResponse res) {
        User user = new User();
        String username = req.getParameter("username").toLowerCase();
        user.setUsername(username);
        user.setName(req.getParameter("firstname"));
        user.setSurname(req.getParameter("lastname"));
        user.setPassword(req.getParameter("password"));
        //The following 2 lines of code are temporary
        if(username.equalsIgnoreCase("cuchilloc")) user.setAdmin(true);
        else user.setAdmin(false);

        Session session = HibernateUtil.getSessionFactory().openSession();
        user.setSession(session);

        UserDAO.getInstance().addUser(session, user);

        req.getSession().setAttribute("user", user);
        return "/index.jsp";
    }

    private String logIn(HttpServletRequest req, HttpServletResponse res) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = UserDAO.getInstance().getUser(session, req.getParameter("username").toLowerCase());
        if(user != null){
            if(user.getPassword().equalsIgnoreCase(req.getParameter("password"))){
                user.setSession(session);
                req.getSession().setAttribute("user", user);
            }else{
                req.setAttribute("badAccess", true);
                req.setAttribute("badAccessMessage", Messages.USER_BAD_PASSWORD);
            }
        }else{
            req.setAttribute("badAccess", true);
            req.setAttribute("badAccessMessage", Messages.USER_NOT_EXISTS);
        }
        return "/index.jsp";
    }

    private String signUp(HttpServletRequest req, HttpServletResponse res) {
        List<String> userList = UserDAO.getInstance().getUsers(HibernateUtil.getGuestSession());
        StringBuffer list = new StringBuffer();
        //Writing userList to a javascript format e.j = ['username1','username2']
        list.append("[");
        for (String user : userList) {
            list.append("'"+user+"',");
        }
        list.append("]");
        req.setAttribute("userList", list);
        req.setAttribute("shortUser", Messages.USER_TOO_SHORT);
        req.setAttribute("usedUser", Messages.USER_USED);
        req.setAttribute("goodUser", Messages.USER_AVAILABLE);
        req.setAttribute("checkingUser", Messages.USER_CHECKING);
        req.setAttribute("pwdNotMatch", Messages.PWD_MATCH);
        return "/signup/signupForm.jsp";
    }
}
