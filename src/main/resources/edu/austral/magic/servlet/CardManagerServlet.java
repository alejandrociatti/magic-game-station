package edu.austral.magic.servlet;

import edu.austral.magic.components.*;
import edu.austral.magic.dao.CardDAO;
import edu.austral.magic.hibernate.HibernateUtil;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * User: alejandro
 * Date: 4/5/13
 * Time: 10:25 AM
 */

@WebServlet(name = "Card Manager", urlPatterns = {"/CardManager"})
public class CardManagerServlet extends ActionHandlerServlet {


    protected void handleActionGet(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws IOException {
        //checks request for desired action
        switch (actionID){
            case CARD_GET: sendCardJSON(req, resp);
            case CARD_SEARCH: searchCard(req, resp);
        }
    }

    @Override
    protected String handleActionPost(HttpServletRequest req, HttpServletResponse resp, ActionID actionID)
            throws ServletException, IOException {
        //checks request for desired action
        switch (actionID){
            case CARD_ADD: return addCard(req, resp);
            case CARD_DELETE: return deleteCard(req, resp);
            case CARD_MODIFY: return modifyCard(req, resp);
            default: return null;
        }
    }

    //GET Action handlers
    private void sendCardJSON(HttpServletRequest req, HttpServletResponse res) throws IOException{
        res.setContentType("text/plain");
        res.setCharacterEncoding("UTF-8");
        //Get Hibernate Session from User to find card matching request id
        Session session = ((User)req.getSession().getAttribute("user")).getSession();
        String idFromReq = req.getParameter("id");
        if(!idFromReq.equalsIgnoreCase("") && idFromReq!=null){
            long ID = Long.parseLong(idFromReq);
            Card card = CardDAO.getCardDAO().getCard(session, ID);
            //Create new JSONObject, add the requested card attributes and send it back.
            JSONObject jsonCard = new JSONObject();
            try {
                jsonCard.put("name", card.getName());
                if(card.getCMC() != null){
                    jsonCard.put("CMC", card.getCMC());
                }else{
                    jsonCard.put("CMC", "null");
                }
                jsonCard.put("magicSet", card.getMagicSet());
                if(card.getPower() != null){
                    jsonCard.put("power", card.getPower());
                }else{
                    jsonCard.put("power", "null");
                }
                if(card.getToughness() != null){
                    jsonCard.put("toughness", card.getToughness());
                }else{
                    jsonCard.put("toughness", "null");
                }
                jsonCard.put("rules",card.getRules().replace("\\",""));
                jsonCard.put("subType", card.getSubtype());
                jsonCard.put("id",card.getId());
            } catch (JSONException e) {
                System.err.println("JSON append failed. Wait, what?");
            }
            //add json object to the response
            res.setContentType("application/json");
            String jsonString = jsonCard.toString();
            System.out.println(jsonString);
            res.getWriter().write(jsonString);
        }
    }

    private void searchCard(HttpServletRequest req, HttpServletResponse resp) {
        String name = "";
        Type type;
        String gotType = req.getParameter("type").toUpperCase();
        String subType = "";
        //If no Type is selected, Type is set to UNKNOWN
        if(gotType != null && !gotType.equalsIgnoreCase("")){
            type = Type.valueOf(gotType);
        }else{
            type = Type.UNKNOWN;
        }
        String rules = "";
        String set = req.getParameter("set");
        String byWhat = req.getParameter("byWhat");
        boolean exact = req.getParameter("exact")!=null;
        boolean exclude = req.getParameter("exclude")!=null;
        //Checks what's the search option selected and modifies the appropiate value to be searched.
        if(byWhat.equalsIgnoreCase("name")) name = req.getParameter("search");
        else if(byWhat.equalsIgnoreCase("type")) subType = req.getParameter("search");
        else rules = req.getParameter("search");
        //Makes positive and negative CMC search Strings according to options chosen.
        StringBuffer CMCBuffer = new StringBuffer();
        StringBuffer negateCMC = new StringBuffer();
        if(exact){
            if(req.getParameter("whiteMana")!=null) CMCBuffer.append("%W");
            if(req.getParameter("blueMana")!=null) CMCBuffer.append("%U");
            if(req.getParameter("blackMana")!=null) CMCBuffer.append("%B");
            if(req.getParameter("redMana")!=null) CMCBuffer.append("%R");
            if(req.getParameter("greenMana")!=null) CMCBuffer.append("%G");
        }else{
            if(req.getParameter("whiteMana")!=null) CMCBuffer.append("W");
            if(req.getParameter("blueMana")!=null) CMCBuffer.append("U");
            if(req.getParameter("blackMana")!=null) CMCBuffer.append("B");
            if(req.getParameter("redMana")!=null) CMCBuffer.append("R");
            if(req.getParameter("greenMana")!=null) CMCBuffer.append("G");
        }
        if(exclude){
            if(req.getParameter("whiteMana")==null) negateCMC.append("W");
            if(req.getParameter("blueMana")==null) negateCMC.append("U");
            if(req.getParameter("blackMana")==null) negateCMC.append("B");
            if(req.getParameter("redMana")==null) negateCMC.append("R");
            if(req.getParameter("greenMana")==null) negateCMC.append("G");
        }
        //fetch hibernate Session from user that made the request
        Session session = HibernateUtil.getGuestSession();
        //get results from processed form data:
        List<Card> cards = CardDAO.getCardDAO().searchCards(session, name, type, subType, rules, set,
                CMCBuffer.toString(), negateCMC.toString(), exact, exclude);
        JSONArray result = new JSONArray();
        try{
            for (Card card : cards) {
                JSONObject toAdd = new JSONObject();
                toAdd.put("id", card.getId());
                toAdd.put("name", card.getName());
                toAdd.put("type", card.getType());
                toAdd.put("magicSet", card.getMagicSet());
                result.put(toAdd);
            }
            JSONObject toSend = new JSONObject();
            toSend.put("results", result);
            resp.getWriter().write(toSend.toString());
        }catch(JSONException e){
            System.err.println("Could NOT write JSONObject to return to finder's result");
            System.err.println(e);
        } catch (IOException e) {
            System.err.println("Could NOT write to finder's response for results handling");
            System.err.println(e);
        }
    }


    //POST Action handlers
    private String modifyCard(HttpServletRequest req, HttpServletResponse resp) {
        Session session = ((User)req.getSession().getAttribute("user")).getSession();
        Long ID = Long.parseLong(req.getParameter("id"));
        Card toModify = CardDAO.getCardDAO().getCard(session, ID);
        toModify.setName(req.getParameter("name"));
        toModify.setCMC(req.getParameter("CMC"));
        toModify.setType(Type.valueOf(req.getParameter("type").toUpperCase()));
        toModify.setSubtype(req.getParameter("subtype"));
        toModify.setRules(req.getParameter("rules"));
        toModify.setPower(req.getParameter("power"));
        toModify.setToughness(req.getParameter("toughness"));
        toModify.setMagicSet(req.getParameter("set"));
        toModify.setRarity(Rarity.valueOf(req.getParameter("rarity")));
        toModify.setColor(CardColor.valueOf(req.getParameter("color")));

        session.save(toModify);

        return "/logged/admin/modifyCards.jsp";
    }

    private String deleteCard(HttpServletRequest req, HttpServletResponse resp) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    private String addCard(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Card card = new Card();
        card.setName(req.getParameter("name"));
        card.setType(Type.valueOf(req.getParameter("type")));
        card.setRules(req.getParameter("rules"));
        card.setText(req.getParameter("text"));
        card.setCMC(req.getParameter("CMC"));
        card.setPower(req.getParameter("power"));
        card.setToughness(req.getParameter("toughness"));
        card.setMagicSet(req.getParameter("set"));
        card.setRarity(checkRarity(req.getParameter("rarity")));
        Session session = ((User)req.getSession().getAttribute("user")).getSession();
        CardDAO.getCardDAO().addCard(session, card);
        req.setAttribute("card", card);
        return "/admin/cardAdded.jsp";
    }

    private Rarity checkRarity(String rarity) {
        return Rarity.valueOf(rarity.toUpperCase());
    }
}
