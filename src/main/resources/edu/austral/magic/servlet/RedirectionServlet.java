package edu.austral.magic.servlet;

import edu.austral.magic.components.User;
import edu.austral.magic.dao.CardDAO;
import org.hibernate.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: alejandro
 * Date: 5/15/13
 * Time: 1:22 PM
 */

@WebServlet(name = "Redirection", urlPatterns={"/search"})
public class RedirectionServlet extends ActionHandlerServlet {
    @Override
    protected void handleActionGet(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws IOException {
        switch (actionID){
            case UNKNOWN: System.out.println("GET request with UNKNOWN action performed");
        }
    }

    @Override
    protected String handleActionPost(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws ServletException, IOException {
        switch (actionID){
            case REDIRECT_SEARCH: return goToFinder(req, resp);
            default: return null;
        }
    }

    private String goToFinder(HttpServletRequest req, HttpServletResponse resp) {
        Session session = ((User)req.getSession().getAttribute("user")).getSession();
        req.setAttribute("sets", CardDAO.getCardDAO().getSets(session));
        return "/logged/search/finder.jsp";
    }
}
