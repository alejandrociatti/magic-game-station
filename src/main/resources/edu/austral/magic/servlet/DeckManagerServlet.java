package edu.austral.magic.servlet;

import edu.austral.magic.components.Card;
import edu.austral.magic.components.Deck;
import edu.austral.magic.components.User;
import edu.austral.magic.dao.CardDAO;
import edu.austral.magic.dao.UserDAO;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: alejandro
 * Date: 5/14/13
 * Time: 11:15 PM
 */
@WebServlet(name = "Deck Manager", urlPatterns = {"/deckManager"})
public class DeckManagerServlet extends ActionHandlerServlet{
    @Override
    protected void handleActionGet(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws IOException {
        switch (actionID){
            case UNKNOWN:
                System.out.println("GET request with UNKNOWN action performed");
        }
    }

    @Override
    protected String handleActionPost(HttpServletRequest req, HttpServletResponse resp, ActionID actionID) throws ServletException, IOException {
        switch (actionID){
            case DECK_CREATE: return createDeck(req, resp);
            case DECK_EDIT: return editDeck(req, resp);
            default: return null;
        }
    }

    private String createDeck(HttpServletRequest req, HttpServletResponse resp) {
        try {
            //get DeckJSON from req (it is a parameter (String))
            JSONObject deckJSON = new JSONObject(new JSONTokener(req.getParameter("deck")));
            //Fetch Hibernate Session from req Session
            User creator = (User) req.getSession().getAttribute("user");
            Session session = creator.getSession();
            Deck deck = new Deck();
            //Create deck attributes with the deckJSON's contents
            List<Card> mainDeck = getCardListFromJSONArray(session, (JSONArray) deckJSON.get("deck"));
            List<Card> sideboard = getCardListFromJSONArray(session, (JSONArray) deckJSON.get("sideboard"));
            String deckName = req.getParameter("deckName");
            deck.setDeck(mainDeck);
            deck.setSideboard(sideboard);
            deck.setName(deckName);
            //Add deck to user NOW because it won't load from database as it's already loaded.
            deck.setCreator(creator);
            creator.setDeck(deck);
            //Save user with new deck to database. (should save the deck)
            UserDAO.getInstance().saveUser(session, creator);
        } catch (JSONException e) {
            System.err.println("Could not load deck from JSON");
            System.err.println("Either req parameter 'deck' was invalid JSON or couldn't get cardJSON inside deckJSON");
            System.err.println(e);
        }

        return "/logged/deckedit/deckList.jsp";
    }

    private String editDeck(HttpServletRequest req, HttpServletResponse resp){


        return "/logged/deckedit/deckEditor.jsp";
    }

    /**
     * This method is used to create Card list from JSONArray of JSONObject containing valid id & qty keys
     *
     * @param session org.Hibernate.Session hibernate session to fetch cards from database
     * @param cards org.json.JSONArray JSONArray of JSONObject with id & qty keys
     * @return List<Card> containing each card from JSONArray (multiple of each if qty > 1)
     * @throws JSONException if it cannot get a JSONObject in the array
     */
    private List<Card> getCardListFromJSONArray(Session session, JSONArray cards) throws JSONException {
        List<Card> list = new ArrayList<Card>(cards.length());
        for(int i = 0; i < cards.length(); i++){
            JSONObject cardJSON = cards.getJSONObject(i);
            Long cardID = (long) ((Integer)cardJSON.get("id"));
            int quantity = (Integer)cardJSON.get("qty");
            Card toAdd = CardDAO.getCardDAO().getCard(session, cardID);
            for(int j = 0; j < quantity; j++){
                list.add(toAdd);
            }
        }
        return list;
    }
}
