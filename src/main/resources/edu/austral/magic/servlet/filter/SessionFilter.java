package edu.austral.magic.servlet.filter;

import edu.austral.magic.messages.Messages;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User: alejandro
 * Date: 4/24/13
 * Time: 5:16 PM
 */

@WebFilter(filterName = "logged-only filter" ,urlPatterns = {"/logged/*", "/logout"})
public class SessionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        String testParam = filterConfig.getInitParameter("test-param");

        System.out.println("Test Param: " + testParam);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
                        throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpSession session = request.getSession();
        System.out.println(request.getRequestURL());
        if(session == null || session.getAttribute("user") == null){
            servletRequest.setAttribute("badAccess", true);
            servletRequest.setAttribute("badAccessMessage", Messages.USER_ONLY);
            ((HttpServletResponse)servletResponse).sendRedirect("/MGS");
        }else{
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        //Do nothing
    }
}
