package edu.austral.magic.servlet.filter;

import edu.austral.magic.components.User;
import edu.austral.magic.messages.Messages;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User: alejandro
 * Date: 6/4/13
 * Time: 9:05 PM
 */
@WebFilter(filterName = "admin-only-filter", urlPatterns = {"/logged/admin/*"})
public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        String testParam = filterConfig.getInitParameter("test-param");

        System.out.println("Test Param: " + testParam);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpSession session = request.getSession();
        if(!((User)session.getAttribute("user")).isAdmin()){
            servletRequest.setAttribute("badAccess", true);
            servletRequest.setAttribute("badAccessMessage", Messages.ADMIN_ONLY);
            ((HttpServletResponse)servletResponse).sendRedirect("/MGS/login.jsp");
        }else{
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        //Do nothing
    }
}
