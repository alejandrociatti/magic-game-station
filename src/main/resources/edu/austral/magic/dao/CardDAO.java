
package edu.austral.magic.dao;

import edu.austral.magic.components.Card;
import edu.austral.magic.components.CardColor;
import edu.austral.magic.components.Rarity;
import edu.austral.magic.components.Type;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * User: alejandro
 * Date: 4/12/13
 * Time: 9:40 AM
 */
public class CardDAO extends DataAO{
    private static CardDAO ourInstance = new CardDAO();

    private CardDAO(){}

    public static CardDAO getCardDAO(){
        return ourInstance;
    }

    public void addCard(Session session, Card card){
        super.beginTransaction(session);
        session.save(card);
        super.endTransaction(session);
    }

    public void deleteCard(Session session, Card card){
        super.beginTransaction(session);
        session.delete(card);
        super.endTransaction(session);
    }

    public List<Card> listCards(Session session){
        super.beginTransaction(session);
        Query query = session.createQuery("from Card");
        List<Card> list = query.list();
        super.endTransaction(session);
        return list;
    }

    public List<Card> searchCards(Session session, String name, Type type, String subType, String rules, String set,
                                  String CMC, String negateCMC, boolean exact, boolean exclude){
        super.beginTransaction(session);
        Criteria criteria = session.createCriteria(Card.class);
        criteria.add(Restrictions.like("name","%"+name+"%"));
        criteria.add(Restrictions.like("subtype","%"+subType+"%"));
        criteria.add(Restrictions.like("rules","%"+rules+"%"));
        //If Type is not UNKNOWN, add a restriction to determine it
        if(type != Type.UNKNOWN) criteria.add(Restrictions.eq("type", type));
        //If no set is selected, then no magicSet Restriction is added
        if(!set.equalsIgnoreCase("")) criteria.add(Restrictions.eq("magicSet",set));
        //If CMC search is EXACT it adds a single restriction, otherwise, a series of OR restrictions (5 max)
        if(exact){
            criteria.add(Restrictions.like("CMC",CMC+"%"));
        }else if(!CMC.equalsIgnoreCase("")){
            Criterion criterion = Restrictions.like("CMC", "%" + CMC.substring(0, 1) + "%");
            char[] mana = CMC.substring(1).toCharArray();
            for (char c : mana) {
                criterion = Restrictions.or(criterion, Restrictions.like("CMC","%"+c+"%"));
            }
            criteria.add(criterion);
        }
        //If CMC search is set to EXCLUDE a NOT LIKE restriction is added made of a series of ORs
        if(exclude && !negateCMC.equalsIgnoreCase("")){
            Criterion negativeCrit = Restrictions.like("CMC", "%" + negateCMC.substring(0, 1) + "%");
            char[] negateMana = negateCMC.substring(1).toCharArray();
            for (char c : negateMana) {
                negativeCrit = Restrictions.or(negativeCrit, Restrictions.like("CMC","%"+c+"%"));
            }
            negativeCrit = Restrictions.not(negativeCrit);
            criteria.add(negativeCrit);
        }
        List<Card> list = criteria.list();
        super.endTransaction(session);
        return list;
    }

    public void modifyCard(Session session, Long id, String attributeName, String attribute){
        CardAttribute attributeType;
        try{
            attributeType = CardAttribute.valueOf(attributeName);
            super.beginTransaction(session);
            Card card = (Card)session.get(Card.class, id);
            switch (attributeType){
                case COLOR: card.setColor(CardColor.valueOf(attribute));
                case CMC: card.setCMC(attribute);
                case NAME: card.setName(attribute);
                case POWER: card.setPower(attribute);
                case RARITY: card.setRarity(Rarity.valueOf(attribute));
                case RULES: card.setRules(attribute);
                case SET: card.setMagicSet(attribute);
                case TEXT: card.setText(attribute);
                case TOUGHNESS: card.setToughness(attribute);
                case TYPE: card.setType(Type.valueOf(attribute));
                case SUBTYPE: card.setSubtype(attribute);
            }
            session.update(card);
            super.endTransaction(session);
        }catch(IllegalArgumentException e){
            System.err.println("No such attribute in Object Card" + e);
        }
    }

    public List<String> getSets(Session session){
        super.beginTransaction(session);
        Query query = session.createQuery("select distinct magicSet from Card ");
        List<String> list = query.list();
        super.endTransaction(session);
        return list;
    }

    public Card getCard(Session session, long id) {
        Query query = session.createQuery("from Card card where card.id=:cardID");
        query.setLong("cardID", id);
        return (Card)query.uniqueResult();
    }


    private enum CardAttribute {
        COLOR, CMC, NAME, POWER, RARITY, RULES, SET, TEXT, TOUGHNESS, TYPE, SUBTYPE
    }
}
