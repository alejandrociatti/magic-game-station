package edu.austral.magic.dao;

import edu.austral.magic.components.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * User: alejandro
 * Date: 4/22/13
 * Time: 7:32 PM
 */
public class UserDAO extends DataAO{
    private static UserDAO ourInstance = new UserDAO();

    private UserDAO(){}

    public static UserDAO getInstance(){
        return ourInstance;
    }

    public void addUser(Session session, User user){
        super.beginTransaction(session);
        session.save(user);
        super.endTransaction(session);
    }

    public List<String> getUsers(Session session){
        super.beginTransaction(session);
        Query query = session.createQuery("select user.username from User user");
        List<String> list = query.list();
        super.endTransaction(session);
        return list;
    }

    public User getUser(Session session, String user) {
        super.beginTransaction(session);
        Query query = session.createQuery("select user from User user where user.username= :yourUser");
        query.setParameter("yourUser", user);
        super.endTransaction(session);
        return (User)query.uniqueResult();
    }

    public String getUsername(Session session, long userID){
        super.beginTransaction(session);
        Query query = session.createQuery("select user.username from User user where user.id = :userID");
        query.setParameter("userID", userID);
        super.endTransaction(session);
        return (String)query.uniqueResult();
    }

    public void saveUser(Session session, User creator) {
        super.beginTransaction(session);
        session.save(creator);
        super.endTransaction(session);
    }
}
