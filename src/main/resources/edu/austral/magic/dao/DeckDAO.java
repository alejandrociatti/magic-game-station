package edu.austral.magic.dao;

import edu.austral.magic.components.Deck;
import org.hibernate.Session;

/**
 * User: alejandro
 * Date: 5/19/13
 * Time: 5:58 PM
 */
public class DeckDAO extends DataAO{
    private static DeckDAO ourInstance = new DeckDAO();

    public static DeckDAO getInstance() {
        return ourInstance;
    }

    private DeckDAO() {}

    public void saveDeck(Session session, Deck deck){
        super.beginTransaction(session);
        session.save(deck);
        super.endTransaction(session);
    }
}
