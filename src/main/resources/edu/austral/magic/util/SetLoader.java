package edu.austral.magic.util;

import edu.austral.magic.components.Card;
import edu.austral.magic.components.CardColor;
import edu.austral.magic.components.Rarity;
import edu.austral.magic.components.Type;
import edu.austral.magic.dao.CardDAO;
import edu.austral.magic.hibernate.HibernateUtil;
import org.hibernate.Session;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * User: alejandro
 * Date: 4/22/13
 * Time: 3:57 PM
 */
public class SetLoader {
    public static void main(String[] args) throws IOException {
        //File file = new File("/home/alejandro/IdeaProjects/MMagic Game Station/CFX.csv");
        File file = new File("C:\\Users\\alumnosfi\\IdeaProjects\\MGS\\CFX.csv");
        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("\\s*\"\\s*|\\s*;\\s*");

        for(int i = 0; i < 26; i++){
            System.out.println("Skipping titles: "+scanner.next());
        }

        while (scanner.hasNext()){
            Card card = new Card();
            card.setName(scanner.next());
            scanner.next();
            scanner.next();
            card.setMagicSet(scanner.next());
            scanner.next();
            scanner.next();
            card.setRarity(Rarity.valueOf(scanner.next()));
            scanner.next();
            scanner.next();
            card.setColor(CardColor.valueOf(scanner.next().toUpperCase()));
            scanner.next();
            scanner.next();
            card.setCMC(scanner.next());
            scanner.next();
            scanner.next();
            //set Power and Toughness
            String PT = scanner.next();
            if(PT.indexOf("/") != -1){
                card.setPower(PT.substring(0, PT.indexOf("/")));
                card.setToughness(PT.substring(PT.indexOf("/")+1));
            }
            scanner.next();
            scanner.next();
            String type = scanner.next()+"  ";
            if(type.contains("Snow")){
                type.substring(4);
                type+="Snow";
            }
            if(type.contains("Legendary")){
                card.setType(Type.LEGENDARY);
                card.setSubtype(type.substring(9).trim());
            }else if(type.contains("Basic")){
                card.setType(Type.BASIC);
                card.setSubtype(type.substring(5).trim());
            }else if(type.contains("Tribal")){
                card.setType(Type.TRIBAL);
                card.setSubtype(type.substring(6).trim());
            }else if(type.contains("Land")){
                card.setType(Type.LAND);
                card.setSubtype(type.substring(4).trim());
            }else if(type.contains("Creature")){
                card.setType(Type.CREATURE);
                if(type.contains("Artifact")){
                    String subtype = "Artifact "+type.substring(16);
                    card.setSubtype(subtype);
                }else{
                    card.setType(Type.CREATURE);
                    card.setSubtype(type.substring(8).trim());
                }
            }else if(type.contains("Artifact")){
                card.setType(Type.ARTIFACT);
                card.setSubtype(type.substring(8).trim());
            }else if(type.contains("Planeswalker")){
                card.setType(Type.PLANESWALKER);
                card.setSubtype(type.substring(12).trim());
            }else if(type.contains("Enchantment")){
                card.setType(Type.ENCHANTMENT);
                card.setSubtype(type.substring(11).trim());
            }else if(type.contains("Instant")){
                card.setType(Type.INSTANT);
                card.setSubtype(type.substring(7).trim());
            }else if(type.contains("Sorcery")){
                card.setType(Type.SORCERY);
                card.setSubtype(type.substring(7).trim());
            }

            scanner.next();
            scanner.next();
            card.setRules(scanner.next());
            scanner.next();
            scanner.next();

            //skip empty 'flavor' field
            scanner.next();
            scanner.next();

            //create Session and save Card to database
            Session session = HibernateUtil.getSessionFactory().openSession();
            CardDAO.getCardDAO().addCard(session, card);
        }
    }
}
