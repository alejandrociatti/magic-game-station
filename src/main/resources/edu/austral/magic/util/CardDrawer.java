package edu.austral.magic.util;

import edu.austral.magic.components.Card;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Paths;

/**
 * User: alejandro
 * Date: 4/30/13
 * Time: 12:04 PM
 */
public class CardDrawer {
    private static CardDrawer ourInstance = new CardDrawer();
    private Document document;

    public static CardDrawer getInstance() {
        return ourInstance;
    }

    private CardDrawer() {
        try{
            File cardXML = Paths.get("card.html").toFile();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            document = documentBuilder.parse(cardXML);
            document.getDocumentElement().normalize();
        } catch(NullPointerException e){

        } catch (ParserConfigurationException e) {

        } catch (SAXException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String draw(Card card){
        Document newDoc = document;
        switch (card.getType()){
            case CREATURE:
                switch(card.getColor()){
                    case W:
                        document.getDocumentElement().setAttribute("style","background-image: url('WhiteCreature.png')");
                    case U:
                        document.getDocumentElement().setAttribute("style","background-image: url('BlueCreature.png')");
                    case B:
                        document.getDocumentElement().setAttribute("style","background-image: url('BlackCreature.png')");
                    case R :
                        document.getDocumentElement().setAttribute("style","background-image: url('RedCreature.png')");
                    case G :
                        document.getDocumentElement().setAttribute("style","background-image: url('GreenCreature.png')");
                    case GLD:
                        document.getDocumentElement().setAttribute("style","background-image: url('GoldCreature.png')");
                    case ART:
                        document.getDocumentElement().setAttribute("style","background-image: url('ArtifactCreature.png')");
                }
            case PLANESWALKER:
                switch(card.getColor()){
                    case W:
                        document.getDocumentElement().setAttribute("style","background-image: url('WhitePW.png')");
                    case U:
                        document.getDocumentElement().setAttribute("style","background-image: url('BluePW.png')");
                    case B:
                        document.getDocumentElement().setAttribute("style","background-image: url('BlackPW.png')");
                    case R :
                        document.getDocumentElement().setAttribute("style","background-image: url('RedPW.png')");
                    case G :
                        document.getDocumentElement().setAttribute("style","background-image: url('GreenPW.png')");
                    case GLD:
                        document.getDocumentElement().setAttribute("style","background-image: url('GoldPW.png')");
                    case ART:
                        document.getDocumentElement().setAttribute("style","background-image: url('ArtifactPW.png')");
                }
            case BASIC:
                document.getDocumentElement().setAttribute("style","background-image: url('Land.png')");
            case LAND:
                document.getDocumentElement().setAttribute("style","background-image: url('Land.png')");

        }


        //Return modified XML as a String
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        StringWriter writer = new StringWriter();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            Source source = new DOMSource(document);
            transformer.transform(source, new StreamResult(writer));
        } catch (TransformerConfigurationException e) {
            System.err.println("Could NOT create XML Transformer");
        } catch (TransformerException e) {
            System.err.println("Could NOT transform document card.html to String");
        }
        return writer.getBuffer().toString().replaceAll("\n|\r", "");
    }

    public static void main(String[] args) {
        System.out.println(CardDrawer.getInstance().draw(new Card()));
    }
}
