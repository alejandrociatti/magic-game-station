package edu.austral.magic.components;

import javax.persistence.*;
import java.util.List;

/**
 * User: alejandro
 * Date: 5/14/13
 * Time: 11:00 PM
 */
@Entity
@Table(name = "DECK")
public class Deck {
    @Id
    @GeneratedValue
    @Column(name = "DECK_ID")
    private long deckId;
    @Column(name = "name")
    private String name;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "DECK_CARD",joinColumns = {
                @JoinColumn(name = "DECK_ID")}, inverseJoinColumns = {
                @JoinColumn(name = "CARD_ID")})
    private List<Card> deck;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "SIDEBOARD_CARD", joinColumns = {
            @JoinColumn(name = "DECK_ID")}, inverseJoinColumns = {
            @JoinColumn(name = "CARD_ID")})
    private List<Card> sideboard;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User creator;

    public Deck(){}

    public Deck(String name, List<Card> deck, List<Card> sideboard){
        this.name = name;
        this.deck = deck;
        this.sideboard = sideboard;
    }

    public long getDeckId() {
        return deckId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public List<Card> getSideboard() {
        return sideboard;
    }

    public void setSideboard(List<Card> sideboard) {
        this.sideboard = sideboard;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
