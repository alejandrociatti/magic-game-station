package edu.austral.magic.components;

/**
 * User: alejandro
 * Date: 4/30/13
 * Time: 5:21 PM
 */
public enum Type {
    ARTIFACT, BASIC, CREATURE, ENCHANTMENT, INSTANT, LAND, LEGENDARY, PLANESWALKER, SORCERY, TRIBAL, UNKNOWN
}
