package edu.austral.magic.components.game;

import edu.austral.magic.components.Deck;

/**
 * User: alejandro
 * Date: 6/19/13
 * Time: 11:39 AM
 */
public class Player {
    private String username;
    private int gameID;
    private Deck deck;

    public Player(String username, Deck deck, int gameID){
        this.username = username;
        this.deck = deck;
        this.gameID = gameID;
    }

    public String getUsername() {
        return username;
    }

    public int getGameID() {
        return gameID;
    }
}
