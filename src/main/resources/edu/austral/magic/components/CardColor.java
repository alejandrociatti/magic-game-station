package edu.austral.magic.components;

/**
 * User: alejandro
 * Date: 4/30/13
 * Time: 5:32 PM
 */
public enum CardColor {
    W, U, R, B, G, LND, GLD, ART
}
