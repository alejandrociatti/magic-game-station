package edu.austral.magic.components;

/**
 * User: alejandro
 * Date: 4/5/13
 * Time: 9:59 AM
 */
public enum Rarity {
    COMMON, C, UNCOMMON, U, RARE, R, MYTHIC, M
}
