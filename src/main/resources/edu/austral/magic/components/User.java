package edu.austral.magic.components;

import org.hibernate.Session;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * User: alejandro
 * Date: 4/3/13
 * Time: 8:07 PM
 */
@Entity
@Table(name = "USER")
public class User {
    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private long id;
    @Column(name = "USERNAME", unique=true)
    private String username;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SURNAME")
    private String surname;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "ADMIN_STATUS")
    private boolean isAdmin;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "creator", orphanRemoval = true)
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Deck> decks;
    @Transient
    private Session session;

    public User(){}

    public User(String username, String name, String surname, String password, boolean isAdmin, List<Deck> decks){
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.isAdmin = isAdmin;
        this.decks = decks;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public List<Deck> getDecks() {
        return decks;
    }

    public void setDecks(List<Deck> decks) {
        this.decks = decks;
    }

    public void setDeck(Deck deck){
        if(decks == null){
            decks = new ArrayList<Deck>(1);
            decks.add(deck);
        }else{
        decks.add(deck);
        }
    }

    @Override
    public String toString(){
        return name;
    }
}
