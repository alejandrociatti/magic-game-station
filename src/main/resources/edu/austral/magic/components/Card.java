package edu.austral.magic.components;


import edu.austral.magic.util.CardDrawer;

import javax.persistence.*;
import java.util.List;

/**
 * User: alejandro
 * Date: 4/5/13
 * Time: 9:55 AM
 */

@Entity
@Table(name = "CARD")
public class Card {
    @Id
    @GeneratedValue
    @Column(name = "CARD_ID")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "COLOR")
    private CardColor color;
    @Column(name = "CMC")
    private String CMC;
    @Column(name = "TYPE")
    private Type type;
    @Column(name = "SUBTYPE")
    private String subtype;
    @Column(name = "RULES", length = 1000)
    private String rules;
    @Column(name = "TEXT")
    private String text;
    @Column(name = "CARD_POWER")
    private String power;
    @Column(name = "CARD_TOUGHNESS")
    private String toughness;
    @Column(name = "MAGIC_SET")
    private String magicSet;
    @Column(name = "RARITY")
    @Enumerated(EnumType.STRING)
    private Rarity rarity;
    @ManyToMany(mappedBy = "deck")
    List<Deck> main;
    @ManyToMany(mappedBy = "sideboard")
    List<Deck> sideboard;

    public Card(){}

    public Card(String name, CardColor color, String CMC, Type type, String subtype, String rules, String text,
                String power, String toughness, String magicSet, Rarity rarity) {
        this.name = name;
        this.color = color;
        this.CMC = CMC;
        this.type = type;
        this.subtype = subtype;
        this.rules = rules;
        this.text = text;
        this.power = power;
        this.toughness = toughness;
        this.magicSet = magicSet;
        this.rarity = rarity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCMC() {
        return CMC;
    }

    public void setCMC(String CMC) {
        this.CMC = CMC;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getToughness() {
        return toughness;
    }

    public void setToughness(String toughness) {
        this.toughness = toughness;
    }

    public String getMagicSet() {
        return magicSet;
    }

    public void setMagicSet(String set) {
        this.magicSet = set;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public CardColor getColor() {
        return color;
    }

    public void setColor(CardColor color) {
        this.color = color;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String drawXML(){
        return CardDrawer.getInstance().draw(this);
    }

    public String drawImage(){
        return "<img class=\"cardImage\" src=\"/MGS/resources/"+magicSet+"/"+name+".full.jpg\">";
    }

    public List<Deck> getMain() {
        return main;
    }

    public void setMain(List<Deck> main) {
        this.main = main;
    }

    public List<Deck> getSideboard() {
        return sideboard;
    }

    public void setSideboard(List<Deck> sideboard) {
        this.sideboard = sideboard;
    }
}
