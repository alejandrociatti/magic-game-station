package edu.austral.magic.messages;

/**
 * User: alejandro
 * Date: 4/24/13
 * Time: 9:53 AM
 */
public class Messages {

    public static String USER_TOO_SHORT = "Username must be longer than 4 characters.";

    public static String USER_USED = "Username already in use.";

    public static String USER_CHECKING = "Checking availability...";

    public static String USER_AVAILABLE = "Username available!";

    public static String USER_BAD_PASSWORD = "You entered an invalid password when logging in";

    public static String USER_NOT_EXISTS = "The username you tried logging in with does not exist";

    public static String PWD_MATCH = "Password does not match.";

    public static String ADMIN_ONLY = "You tried entering an admin-only part of the site, you've been redirected to the home page";
    public static String USER_ONLY = "You tried entering an user-only part of the sit, you've been redirected to the home page";
}
